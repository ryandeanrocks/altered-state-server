import json
import ryde
from ryde import r_network
from ryde import r_logging
from ryde import r_account
from ryde import r_spacetime
from ryde.r_account import r_character
from ryde import r_math
from ryde.r_game.inventory import *
from ryde.r_game.item import *
from time import sleep
import os


server_domain = r_network.get_local_ip()
server_port = 49866

client_version = 1

server_log = r_logging.Log("Altered State Master Server", True, False, False)
server_log.entry("[MASTER] Starting Altered State Master Server")

cached_accounts = []
server_list = []

total_drinks = 0
total_pizza = 0

def dead_on_tick(c):
    if c.isServer:
        server_log.entry(("[DEAD][SERVER] %s:%d" % (c.ip, c.port)))
        server_list.remove(c.ip)
        # c.thread.thread.join()
        # server.clients.remove(c)
        c.shouldStop = True
    else:
        server_log.entry("[TICK] %s:%d dead on tick. Removing from server." % (c.ip, c.port))
        # c.thread.thread.join()
        # server.clients.remove(c)
        c.shouldStop = True

def client_alter(c):
    if c.account is not None:
        server_log.entry("[TICK][" + c.account.oculusID + "] Reading alters")
        for alter in c.account.alters:
            command_handler(c, alter)

        c.account.clear_alters()
    else:
        found = False
        for s in server_list:
            if s == c.ip:
                server_log.entry("[TICK][GAME] Server: %s" % c.ip)

                if r_spacetime.When().milli() - c.lastMessage > 60000:
                    server_log.entry("[TICK][GAME] Server: %s has timed out!" % c.ip)
                    c.shouldStop = True
                else:
                    timeLeft = r_spacetime.When().milli() - c.lastMessage
                    # server_log.entry("Time left: %d" % timeLeft)

                found = True
                break

        if not found:
            server_log.entry("[TICK] %s:%d  has not logged in yet." % (c.ip, c.port))

def client_state(c):
    if c.account is not None:
        server_log.entry("[TICK][" + c.account.oculusID + "] Sending states")
        for state in c.account.states:
            c.send(state, True)

        c.account.clear_states()

def master_tick(server):
    server_log.entry("[TICK] Starting master tick...")

    # Survey server client alters
    for c in server.clients:
        if c.isReady:
            if not c.isAlive:
                dead_on_tick(c)
            else:
                client_alter(c)
        else:
            server_log.entry("[TICK] %s:%d isn't ready." % (c.ip, c.port))

    # State world to clients
    for c in server.clients:
        if c.isReady:
            if not c.isAlive:
                dead_on_tick(c)
            else:
                client_state(c)
        else:
            server_log.entry("[TICK] %s:%d isn't ready." % (c.ip, c.port))

    # Save cached accounts to files
    if not os.path.exists('saves'):
        os.makedirs('saves')

    for account in cached_accounts:
        char = account.character
        if char is None:
            continue
            # char = account.characterList[0]

        # Save data to file need to wrap to function
        char_string = "{\"oculus\":\"" + account.oculusID + "\",\"donations\":{\"drink\":%d,\"pizza\":%d},\"player\":" % (account.drink, account.pizza)

        char_string += "{\"character\":{\"id\":%d,\"name\":\"%s\"," % (char.id, char.name)

        char_string += "\"x\":%f,\"y\":%f,\"z\":%f," % (char.pos[0], char.pos[1], char.pos[2])

        char_string += "\"sx\":%f,\"sy\":%f,\"sz\":%f}," % (char.spawn[0], char.spawn[1], char.spawn[2])

        char_string += "\"inventory\":["

        for num, slot in enumerate(char.inventory.slots):
            if slot.base is None:
                char_string += "{\"slot\":%d,\"name\":\"Empty\"}" % num
                if char.inventory.slots[-1] != slot:
                    char_string += ","
            else:
                char_string += "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                    num,
                    slot.base.name,
                    slot.base.stackable,
                    slot.quantity()
                )
                if char.inventory.slots[-1] != slot:
                    char_string += ","

        char_string += "],"

        char_string += "\"bank\":["
        for num, slot in enumerate(char.bank.slots):
            if slot.base is None:
                char_string += "{\"slot\":%d,\"name\":\"Empty\"}" % num
                if char.bank.slots[-1] != slot:
                    char_string += ","
            else:
                char_string += "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                    num,
                    slot.base.name,
                    slot.base.stackable,
                    slot.quantity()
                )
                if char.bank.slots[-1] != slot:
                    char_string += ","

        char_string += "],"

        char_string += "\"equipped\":["
        for num, slot in enumerate(char.equipped.slots):
            if slot.base is None:
                char_string += "{\"slot\":%d,\"name\":\"Empty\"}" % num
                if char.equipped.slots[-1] != slot:
                    char_string += ","
            else:
                char_string += "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                    num,
                    slot.base.name,
                    slot.base.stackable,
                    slot.quantity()
                )
                if char.equipped.slots[-1] != slot:
                    char_string += ","

        char_string += "],"

        char_string += "\"skills\":["
        ii = 0
        for name in char.skills:
            char_string += "{\"%s\":{\"level\":%d,\"current\":%d,\"xp\":%d}}" % (name, char.skills[name].level, char.skills[name].current, char.skills[name].xp)
            if ii != len(char.skills) - 1:
                char_string += ","
            ii += 1
        char_string += "],"
        # Quests
        char_string += "\"quests\":["
        for quest in char.quests:
            char_string += "{\"%s\":{\"stage\":%d,\"step\":%d}}" % (quest.name, quest.stage, quest.step)
            if char.quests[-1] != quest:
                char_string += ","

        char_string += "]"

        char_string += "}}"

        f = open("saves/%s.assav" % account.oculusID, 'w')
        f.write(char_string)
        f.close()

        server_log.entry("[SAVED] " + char.name)


def hello_handler(client, hello):
    id = hello["oculusID"]
    version = hello["version"]
    account = None

    if version < client_version:
        server_log.entry("[ERROR] Client version is lower than allowed")
        response = "{\"version\":{\"client_version\":%d}}" % (client_version)
        client.send(response)
        return

    for a in cached_accounts:
        if a.oculusID == id:
            server_log.entry("[COMMAND] Cached Account")
            account = a

    if account is None:
        server_log.entry("[COMMAND] New Account")
        account = r_account.Account(id)

        cached_accounts.append(account)

    client.set_account(account)

    account.character = None

    response = "{\"title\":{\"servers\":["

    for i, s in enumerate(server_list):
        response += "\"" + s + "\""

        if i < len(server_list) - 1:
            response += ","

    response += "],\"characters\":["

    lastComma = len(account.characterList)
    for c in account.characterList:
        if c is not None:
            response += "\"" + c.name + "\""
            lastComma -= 1
            if lastComma > 0:
                response += ","

    response += "],\"drink\":%d,\"pizza\":%d}}" % (account.drink, account.pizza)

    server_log.entry("[COMMAND] " + response)
    client.send(response)

def login_handler(client, login):
    if login.get("character"):
        slot = int(login["character"]["slot"])

        client.account.character = client.account.characterList[slot]

        character_string = "{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f,\"role\":%d}" % (client.account.character.name,
                                                                             client.account.character.pos[0],
                                                                             client.account.character.pos[1],
                                                                             client.account.character.pos[2],
                                                                                         client.account.character.role)

        response = "{\"state\":{\"player\":" + character_string + "}}"

        server_log.entry(response)
        client.send(response)
        client.shouldStop = True

def purchase_handler(client, purchase):
    global total_drinks, total_pizza
    if purchase == "drink":
        # master_client.send("{\"server\":{\"purchase\":\"drink\",\"oculus\":\"%s\"}}" % client.account.oculusID)
        client.account.add_drink()
        total_drinks += 1

    if purchase == "pizza":
        # master_client.send("{\"server\":{\"purchase\":\"pizza\",\"oculus\":\"%s\"}}" % client.account.oculusID)
        client.account.add_pizza()
        total_pizza += 1

    server_log.entry("[STATS] Drinks: %d Pizza: %d" % (total_drinks, total_pizza))

def logout_handler(client, logout):
    print "[TODO] Handle logout"

def server_handler(client, command):
    if command["server"].get("address"):
        client.isServer = True
        if client.ip not in server_list:
            server_list.append(client.ip)
            print server_list

    if command["server"].get("character"):
        for a in cached_accounts:
            if a.oculusID == command["server"]["character"]["oculus"]:
                client.send("{\"master\":{\"state\":{\"oculus\":\"" + a.oculusID + "\",\"player\":")

                client.send("{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f," % (
                    a.character.name,
                    a.character.pos[0],
                    a.character.pos[1],
                    a.character.pos[2]))

                client.send("\"inventory\":[")

                lastComma = len(a.character.inventory.slots)
                for s in a.character.inventory.slots:
                    if s.base is None:
                        client.send("{}")
                        # c.send("{}")
                    else:
                        client.send("{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}" % (
                            s.base.name,
                            s.base.stackable,
                            s.quantity(),
                            s.base.value
                        ))

                    lastComma -= 1
                    if lastComma > 0:
                        client.send(",")
                        # c.send(",")

                client.send("],")

                client.send("\"bank\":[")

                lastComma = len(a.character.bank.slots)
                for s in a.character.bank.slots:
                    if s.base is None:
                        client.send("{}")
                        # c.send("{}")
                    else:
                        client.send("{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}" % (
                            s.base.name,
                            s.base.stackable,
                            s.quantity(),
                            s.base.value
                        ))

                    lastComma -= 1
                    if lastComma > 0:
                        client.send(",")
                        # c.send(",")

                client.send("],")

                client.send("\"equipped\":[")

                lastComma = len(a.character.equipped.slots)
                for s in a.character.equipped.slots:
                    if s.base is None:
                        client.send("{}")
                        # c.send("{}")
                    else:
                        client.send("{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}" % (
                            s.base.name,
                            s.base.stackable,
                            s.quantity(),
                            s.base.value
                        ))

                    lastComma -= 1
                    if lastComma > 0:
                        client.send(",")
                        # c.send(",")


                client.send("],")

                # Skills
                client.send("\"skills\":[")
                ii = 0
                for name in a.character.skills:
                    client.send("{\"%s\":{\"level\":%d,\"current\":%d,\"xp\":%d}}" % (
                    name, a.character.skills[name].level, a.character.skills[name].current,
                    a.character.skills[name].xp))
                    if ii != len(a.character.skills) - 1:
                        client.send(",")
                    ii += 1
                client.send("]}")

                client.send("}}}")

                break

    if command["server"].get("state"):
        for a in cached_accounts:
            if a.oculusID == command["server"]["state"]["oculus"]:
                # Update player with game servers stats
                # print "Save player %s" % a.oculusID
                a.character.setPosition(command["server"]["state"]["player"]["character"]["x"],
                                        command["server"]["state"]["player"]["character"]["y"],
                                        command["server"]["state"]["player"]["character"]["z"])

                a.character.spawn = [command["server"]["state"]["player"]["character"]["sx"],
                                     command["server"]["state"]["player"]["character"]["sy"],
                                     command["server"]["state"]["player"]["character"]["sz"]]

                for item in command["server"]["state"]["player"]["inventory"]:
                    a.character.inventory.slots[int(item["slot"])] = ItemSlot()
                    if item["name"] != "Empty":
                        for base in item_dictionary:
                            if item["name"] == item_dictionary[base].name:
                                for n in range(0, item["quantity"]):
                                    a.character.inventory.slots[int(item["slot"])].add_to_stack(item_dictionary[base].generate())

                                break

                for item in command["server"]["state"]["player"]["bank"]:
                    a.character.bank.slots[int(item["slot"])] = ItemSlot()
                    if item["name"] != "Empty":
                        for base in item_dictionary:
                            if item["name"] == item_dictionary[base].name:
                                for n in range(0, item["quantity"]):
                                    a.character.bank.slots[int(item["slot"])].add_to_stack(
                                        item_dictionary[base].generate())

                                break

                for item in command["server"]["state"]["player"]["equipped"]:
                    a.character.equipped.slots[int(item["slot"])] = ItemSlot()
                    if item["name"] != "Empty":
                        for base in item_dictionary:
                            if item["name"] == item_dictionary[base].name:
                                for n in range(0, item["quantity"]):
                                    a.character.equipped.slots[int(item["slot"])].add_to_stack(
                                        item_dictionary[base].generate())

                                break

                for skill in a.character.skills:
                    for s in command["server"]["state"]["player"]["skills"]:
                        if s.get(skill):
                            a.character.skills[skill].level = s[skill]["level"]
                            a.character.skills[skill].current = s[skill]["current"]
                            a.character.skills[skill].xp = s[skill]["xp"]
                            break

                for q in command["server"]["state"]["player"]["quests"]:
                    for quest in a.character.quests:
                        if q.get(quest.name):
                            quest.step = q[quest.name]["step"]
                            quest.stage = q[quest.name]["stage"]
                            break

                break

def state_handler(client, state):
    print "[TODO] Add state handler"

def alter_handler(client, alter):
    if alter.get("new"):
        type = alter["new"]

        if type.get("character"):
            name = type["character"]
            slot = int(type["slot"])

            client.account.character = r_account.r_character.Character(name)
            client.account.characterList[slot] = client.account.character
            client.account.character.pos = [277.9059, 266.2097, 9.783813]

            for ii in range(0, 1500):
                client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Copper].generate())

            for ii in range(0, 750):
                client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Silver].generate())

            for ii in range(0, 10):
                client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Gold].generate())

            for ii in range(0, 1):
                client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Platinum].generate())

            for ii in range(0, 100):
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.AspenLog].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CedarLog].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.MapleLog].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CopperOre].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NickelOre].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.IronOre].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CuSword].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.FeSword].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NiSword].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CuShield].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.FeShield].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NiShield].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CuArrow].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.FeArrow].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NiArrow].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CuAxe].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.FeAxe].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NiAxe].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CuPick].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.FePick].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.NiPick].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.AsHilt].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CeHilt].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.MaHilt].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.AsFrame].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CeFrame].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.MaFrame].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.AsShaft].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CeShaft].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.MaShaft].generate())

                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.AsHandle].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.CeHandle].generate())
                client.account.character.bank.addItem(item_dictionary[ItemDictionaryIndex.MaHandle].generate())

                client.account.character.equipped.addItem(item_dictionary[ItemDictionaryIndex.AsCuArrow].generate())

            client.account.character.equipped.addItem(item_dictionary[ItemDictionaryIndex.AsCuSword].generate())

            client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.AsCuShield].generate())

            client.account.character.inventory.to_terminal()
            client.account.character.bank.to_terminal()
            client.account.character.equipped.to_terminal()

            character_string = "{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f}" % (
                client.account.character.name,
                client.account.character.pos[0],
                client.account.character.pos[1],
                client.account.character.pos[2])

            response = "{\"state\":{\"player\":" + character_string + "}}"

            server_log.entry(response)
            client.send(response)
            client.shouldStop = True

def command_handler(client, command):
    # Alters are called before states

    if command.get("purchase"):
        purchase_handler(client, command["purchase"])

    if command.get("server"):
        server_handler(client, command)

    if command.get("hello"):
        server_log.entry("[COMMAND] Hello command detected.")
        hello_handler(client, command["hello"])

    if command.get("login"):
        server_log.entry("Login command detected.")
        login_handler(client, command["login"])

    if command.get("alter"):
        server_log.entry("Alter command detected.")
        alter_handler(client, command["alter"])


server = r_network.TCPServer(server_domain, server_port, command_handler, server_log)

server_log.entry("[MASTER] Waiting on server to start...")
server.start()

wait = 0
while not server.isReady:
    wait += 1
    if wait >= 999999:
        server_log.entry("[MASTER] Server never started")
        quit()

currentTime = r_spacetime.When().milli
lastTick = currentTime()

# cache all account files to memory
for filename in os.listdir("saves"):
    if filename.endswith(".assav"):
        f = open("saves/%s" % filename, 'r')
        char_string = f.readline()
        f.close()

        char = json.loads(char_string)

        account = r_account.Account(os.path.splitext(filename)[0])

        if char.get("donations"):
            account.drink = char["donations"]["drink"]
            account.pizza = char["donations"]["pizza"]

            total_drinks += account.drink
            total_pizza += account.pizza

        account.characterList[0] = r_account.r_character.Character(char["player"]["character"]["name"], char)

        cached_accounts.append(account)

server_log.entry("[STATS] Drinks: %d Pizza: %d" % (total_drinks, total_pizza))

while server.isAlive:
    if currentTime() - lastTick >= 600:
        master_tick(server)
        lastTick = currentTime()

    sleep(0.1)

server.thread.thread.join()
quit()