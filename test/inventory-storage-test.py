import ryde
from ryde import r_network
from ryde import r_logging
from ryde import r_account
from ryde import r_spacetime
from ryde.r_account import r_character
from ryde import r_math
from ryde import r_game
from ryde.r_game import item

test_account = r_account.Account("TestUser")
test_account.character = r_character.Character("testCharacter")
test_account.character.pos = [275.876, 265.8, 9.885021]

item_instance = item.item_dictionary[item.ItemDictionaryIndex.Copper].generate()

print "Item: %s Stackable: %s Value: %d" % (item_instance.base.name,
                                                      item_instance.base.stackable,
                                                      item_instance.base.value)

other_item_instance = item.item_dictionary[item.ItemDictionaryIndex.Silver].generate()
print "Item: %s Stackable: %s Value: %d" % (other_item_instance.base.name,
                                                      other_item_instance.base.stackable,
                                                      other_item_instance.base.value)

test_account.character.inventory.addItem(item_instance)
test_account.character.inventory.addItem(other_item_instance)

print "Slots %d" % len(test_account.character.inventory.slots)
for i,s in enumerate(test_account.character.inventory.slots):
    if s.base is not None:
        print "[%d] Item: %s Stackable: %s Stack: %d Value: %d" % (i,s.base.name,s.base.stackable,s.quantity(),s.base.value)


dropped_item = test_account.character.inventory.removeItem("Silver")

for i,s in enumerate(test_account.character.inventory.slots):
    if s.base is not None:
        print "[%d] Item: %s Stackable: %s Stack: %d Value: %d" % (i,s.base.name,s.base.stackable,s.quantity(),s.base.value)

print "Dropped %s" % dropped_item.base.name