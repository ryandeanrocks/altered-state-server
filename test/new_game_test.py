
import json
import ryde
from ryde import r_network
from ryde.r_network import protocol
from ryde import r_logging
from ryde import r_account
from ryde import r_spacetime
from ryde.r_account import r_character
from ryde import r_math
from ryde.r_game.inventory import *
from ryde.r_game.item import *
from ryde.r_game.store import *
from time import sleep


master_domain = "192.168.99.71"
master_port = 49866

server_domain = r_network.get_local_ip()
server_port = 49867

server_log = r_logging.Log("Altered State Game Server", True, False, False)
server_log.entry("[GAME] Starting Altered State Game Server")

test_account = r_account.Account("TestUser")
test_account.character = r_character.Character("testCharacter")
test_account.character.pos = [275.876, 265.8, 9.885021]

test_npc = r_character.Character("Chicken")
test_npc.spawn = [261.0817, 265.25, 109.2133]
test_npc.shouldSpawn = True

test_npc2 = r_character.Character("Rat")
test_npc2.spawn = [261.0817, 265.25, 109.2133]
test_npc2.shouldSpawn = True

cached_accounts = []
world_items = []
npcs = []
stores = {
    "general": Store("General", [item_dictionary[ItemDictionaryIndex.AsCuSword].generate()])
}

npcs.append(test_npc)
npcs.append(test_npc2)

def client_tick(client):
    server_log.entry("[TICK][" + client.account.oculusID + "] Starting tick...")
    if client.account.character is not None:
        if client.account.character.skills["health"].current <= 0:

            for s in client.account.character.inventory.slots:
                while s.base is not None:
                    instance = s.take_from_stack()

                    if instance is None:
                        break

                    slot = instance.drop(ItemSlot(), [
                        client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                        client.account.character.pos[1] - 1,
                        client.account.character.pos[2] + r_math.randomFloat(-1, 1)])
                    #world_items.append(slot)

                    #protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)


            for s in client.account.character.equipped.slots:
                while s.base is not None:
                    instance = s.take_from_stack()

                    if instance is None:
                        break

                    slot = instance.drop(ItemSlot(), [
                        client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                        client.account.character.pos[1] - 1,
                        client.account.character.pos[2] + r_math.randomFloat(-1, 1)])
                    #world_items.append(slot)

                    #protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)

            client.account.character.player_die()
            protocol.state_player_to_client(client)



turnCount = 25
def game_tick(server):
    server_log.entry("[TICK] Starting game tick...")

    for a in cached_accounts:
        found = False
        for c in server.clients:
            if c.account.oculusID == a.oculusID:
                found = True
                break

        if not found:
            cached_accounts.remove(a)
            print "cleaned account"
            continue

    # Survey server clients
    for c in server.clients:
        if not c.isAlive:
            server_log.entry("[TICK] %s:%d dead on tick. Removing from server." % (c.ip, c.port))
            #c.thread.thread.join()
            #server.clients.remove(c)
            c.shouldStop = True
        else:
            if c.account is not None:
                client_tick(c)
            else:
                server_log.entry("[TICK] %s:%d  has not logged in yet." % (c.ip, c.port))

    for c in server.clients:
        if c.isAlive and c.account is not None:
            # Process all on_tick alters
            for alter in c.account.alters:
                command_handler(c, alter)

            c.account.clear_alters()

    # Sanction altered state

    for n in npcs:
        n.tick(server.clients)


    global turnCount
    if test_account.character is not None:
        if test_account.character.name == "testCharacter":
            pos = list(test_account.character.pos)
            if turnCount <= 0:
                pos[2] = pos[2] - 2.0

                if turnCount == -25:
                    turnCount = 25

            if turnCount > 0:
                pos[2] = pos[2] + 2.0

            turnCount -= 1

            test_account.character.setPosition(pos[0], pos[1], pos[2])


    precount = len(world_items)
    for slot in world_items:
        base_name = "Item"
        if slot.base is not None:
            base_name = slot.base.name
        slot.tick()
        if slot.base is None:
            server_log.entry(base_name + " disintegrated")
            world_items.remove(slot)
            continue

    # BADBADBAD LAZY BAD CODE
    for c in server.clients:
        if precount != len(world_items):
            server_log.entry("Stating world to client.")
            protocol.state_full_world_sync(c)
            protocol.state_world_to_client(c, world_items)

        if c.account.character.needsUpdate:
            protocol.state_skill_to_client(c, "health")
            c.account.character.needsUpdate = False

        protocol.state_npcs_to_client(c, npcs)
        protocol.state_clients_to_client(c, server.clients)
        protocol.state_client_to_master(server.master, c)

        char_string = "{\"state\":{\"character\":{\"id\":%d,\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f,\"health\":%d}}}" % (
            test_account.character.id,
            test_account.character.name,
            test_account.character.pos[0],
            test_account.character.pos[1],
            test_account.character.pos[2],
            test_account.character.skills["health"].current)

        c.send(char_string)

    for n in npcs:
        n.needsUpdate = False

    for c in server.clients:
        if c.isAlive and c.account is not None:
            # Process all on_tick alters
            for state in c.account.states:
                c.send(state, True)

            c.account.clear_states()


def command_handler(client, command):

    if command.get("logout"):
        client.shouldStop = True
        server_log.entry("[TICK] %s:%d  has logged out." % (client.ip, client.port))
        return

    if command.get("master"):
        if command["master"].get("state"):
            if command["master"]["state"].get("oculus"):
                for a in cached_accounts:
                    if a.oculusID == command["master"]["state"]["oculus"]:
                        a.character = r_account.r_character.Character(command["master"]["state"]["player"]["name"])
                        a.character.pos = [command["master"]["state"]["player"]["x"],
                                           command["master"]["state"]["player"]["y"],
                                           command["master"]["state"]["player"]["z"]]

                        for ii in command["master"]["state"]["player"]["inventory"]:
                            if ii.get("name"):
                                for base in item_dictionary:
                                    if ii["name"] == item_dictionary[base].name:
                                        for n in range(0, ii["quantity"]):
                                            a.character.inventory.addItem(item_dictionary[base].generate())
                                        break

                        for ii in command["master"]["state"]["player"]["bank"]:
                            if ii.get("name"):
                                for base in item_dictionary:
                                    if ii["name"] == item_dictionary[base].name:
                                        for n in range(0, ii["quantity"]):
                                            a.character.bank.addItem(item_dictionary[base].generate())
                                        break

                        for ii in command["master"]["state"]["player"]["equipped"]:
                            if ii.get("name"):
                                for base in item_dictionary:
                                    if ii["name"] == item_dictionary[base].name:
                                        for n in range(0, ii["quantity"]):
                                            # print "added " + ii["name"]
                                            test_num = a.character.equipped.addItem(item_dictionary[base].generate())
                                            # print "to %d" % test_num
                                        break

                        for skill in a.character.skills:
                            for s in command["master"]["state"]["player"]["skills"]:
                                if s.get(skill):
                                    a.character.skills[skill].level = s[skill]["level"]
                                    a.character.skills[skill].current = s[skill]["current"]
                                    a.character.skills[skill].xp = s[skill]["xp"]
                                    break

                        for c in server.clients:
                            if c.account == a:

                                protocol.state_player_to_client(c)

                                break

                        break

    if command.get("hello"):
        server_log.entry("[COMMAND] Hello command detected.")

        oculus = command["hello"]["oculusID"]



        found = False
        for ca in cached_accounts:
            if ca.oculusID == oculus:
                client.account = ca
                found = True
                break

        if not found:
            client.account = r_account.Account(oculus)
            cached_accounts.append(client.account)

        server.master.send("{\"server\":{\"character\":{\"oculus\":\"%s\"}}}" % oculus)


    if command.get("state"):
        state = command["state"]

        if state == "inventory":
            protocol.state_inventory_to_client(client)
        if state == "bank":
            server_log.entry("Bank state detected.")
            protocol.state_bank_to_client(client)
        if state == "equipped":
            server_log.entry("Equipped state detected.")
            protocol.state_equipped_to_client(client)
        if state == "skills":
            server_log.entry("Skill state detected.")
            protocol.state_skills_to_client(client)
        if state == "quests":
            server_log.entry("Quest state detected.")
            protocol.state_quests_to_client(client)
        if state == "all":
            server_log.entry("All state detected.")
            protocol.state_inventory_to_client(client)
            protocol.state_bank_to_client(client)
            protocol.state_equipped_to_client(client)
            protocol.state_skills_to_client(client)
            protocol.state_quests_to_client(client)
            protocol.state_npcs_to_client(client, npcs, True)
            protocol.state_full_world_sync(client)
            protocol.state_world_to_client(client, world_items)
            protocol.state_full_sync_complete(client)
        if state == "store":
            name = command["name"]

            if name == "general":
                server_log.entry("General store state.")
                protocol.state_store_to_client(client, stores["general"])


    if command.get("alter"):
        server_log.entry("Alter command detected.")

        alter = command["alter"]

        if alter.get("character"):
            character = alter["character"]

            if character.get("position"):
                position = character["position"]

                x = float(position[0])
                y = float(position[1])
                z = float(position[2])

                client.account.character.setPosition(x, y, z)

                character_string = "{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f}" % (
                    client.account.character.name,
                    client.account.character.pos[0],
                    client.account.character.pos[1],
                    client.account.character.pos[2])

        if alter.get("move"):
            server_log.entry("Move command detected.")
            move = alter["move"]

            if move.get("item"):
                #print "moving item"
                item = move["item"]["name"]
                source = move["item"]["source"]
                target = None
                if move["item"].get("target"):
                    target = move["item"]["target"]
                    server_log.entry("To target: " + target)
                    #print "to a target"

                if source == "inventory":
                    if target is None:
                        slot_num = client.account.character.inventory.hasItem(item)
                        if slot_num is not None:
                            dropped_item = client.account.character.inventory.removeItem(item)
                            if dropped_item is not None:

                                if dropped_item.base.type == 2 and client.account.character.skills["health"].current < client.account.character.skills["health"].level:
                                    client.account.character.skills["health"].current += dropped_item.base.modifier
                                    protocol.state_inventory_slot_to_client(client, slot_num)
                                else:

                                    equipped_it = client.account.character.equipped.addItem(dropped_item)

                                    if equipped_it is not None:
                                        protocol.state_equipped_slot_to_client(client, equipped_it)
                                        protocol.state_inventory_slot_to_client(client, slot_num)

                                    else:
                                        slot = dropped_item.drop(ItemSlot(), [
                                            client.account.character.pos[0] + r_math.randomFloat(-1,1),
                                            client.account.character.pos[1] - 1,
                                            client.account.character.pos[2] + r_math.randomFloat(-1,1)])
                                        world_items.append(slot)
                                        server_log.entry(world_items)

                                        protocol.state_drop_to_clients(server.clients, slot, len(world_items)-1)
                                        protocol.state_inventory_slot_to_client(client, slot_num)

                            else:
                                server_log.entry("Dropped Item NoneType", server_log.EntryType.WARN)

                    elif target == "bank":
                        inventory_slot_num = client.account.character.inventory.hasItem(item)

                        if inventory_slot_num is not None:
                            bank_slot_num = None
                            to_move = client.account.character.inventory.slots[inventory_slot_num].quantity()
                            for instance_index in range(to_move):
                                instance = client.account.character.inventory.slots[inventory_slot_num].take_from_stack()
                                if instance is None:
                                    break

                                bank_slot_num_test = client.account.character.bank.addItem(instance)
                                if bank_slot_num_test is not None:
                                    bank_slot_num = bank_slot_num_test
                                else:
                                    server_log.entry("Bank rejected item.")
                                    client.account.character.inventory.addItem(instance)
                                    break

                            server_log.entry("Source is inventory %d" % inventory_slot_num)
                            server_log.entry("Target is bank %d" % bank_slot_num)
                            protocol.state_bank_slot_to_client(client, bank_slot_num)
                            protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                if source == "store":
                    store = stores[move["item"].get("store")]

                    store_slot_num = store.inventory.hasItem(item)

                    if store_slot_num is not None:
                        bought_item = store.inventory.removeItem(item)
                        if bought_item is not None:
                            stored_slot = client.account.character.inventory.addItem(bought_item)

                            if stored_slot is not None:
                                protocol.state_store_slot_to_client(client, store, store_slot_num)
                                protocol.state_inventory_slot_to_client(client, stored_slot)


                if source == "equipped":
                    slot_num = client.account.character.equipped.hasItem(item)
                    if slot_num is not None:
                        dropped_item = client.account.character.equipped.removeItem(item)
                        if dropped_item is not None:
                            inventory_it = client.account.character.inventory.addItem(dropped_item)

                            if inventory_it is not None:
                                protocol.state_equipped_slot_to_client(client, slot_num)
                                protocol.state_inventory_slot_to_client(client, inventory_it)
                            else:
                                slot = dropped_item.drop(ItemSlot(), [
                                    client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                                    client.account.character.pos[1] - 1,
                                    client.account.character.pos[2] + r_math.randomFloat(-1, 1)])
                                world_items.append(slot)
                                server_log.entry(world_items)

                                protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)
                                protocol.state_equipped_slot_to_client(client, slot_num)


                if source == "bank":
                    if target is None:
                        server_log.entry("No target on bank withdrawal")
                        return
                    elif target == "inventory":
                        bank_slot_num = client.account.character.bank.hasItem(item)
                        if bank_slot_num is not None:
                            instance = client.account.character.bank.slots[bank_slot_num].take_from_stack()
                            if instance is None:
                                return

                            inventory_slot_num = client.account.character.inventory.addItem(instance)
                            if inventory_slot_num is None:
                                server_log.entry("Inventory rejected item.")
                                client.account.character.bank.addItem(instance)
                                return

                            server_log.entry("Source is bank %d" % bank_slot_num)
                            server_log.entry("Target is inventory %d" % inventory_slot_num)
                            protocol.state_inventory_slot_to_client(client, inventory_slot_num)
                            protocol.state_bank_slot_to_client(client, bank_slot_num)



        if alter.get("skill"):

            skill = alter["skill"]

            if skill.get("action"):

                skill_name = skill["action"]["name"]
                context_name = skill["action"]["context"]
                server_log.entry("Context: " + context_name)

                if skill_name == "Woodcutting":
                    lootChance = r_math.randomFloat(0, 9)

                    if lootChance < 1:
                        server_log.entry("Roll: %f = Get loot." % lootChance)
                        item_instance = None
                        gain_amount = 0
                        if context_name == "aspen_tree":
                            server_log.entry("Aspen")
                            item_instance = item_dictionary[ItemDictionaryIndex.AspenLog].generate()
                            gain_amount = 10
                        if context_name == "cedar_tree":
                            server_log.entry("Cedar")
                            item_instance = item_dictionary[ItemDictionaryIndex.CedarLog].generate()
                            gain_amount = 25
                        if context_name == "maple_tree":
                            server_log.entry("Maple")
                            item_instance = item_dictionary[ItemDictionaryIndex.MapleLog].generate()
                            gain_amount = 42

                        if item_instance is None:
                            server_log.entry("Context not recognized.")
                            return

                        slotNum = client.account.character.inventory.addItem(item_instance)

                        client.account.character.skills["woodcutting"].gain(gain_amount)
                        client.send(client.account.character.skills["woodcutting"].state())

                        response = "{\"reward\":{\"skill\":\"Woodcutting\",\"reward\":{\"xp\":%d,\"item\":\"Log\"}}}" % gain_amount

                        if slotNum is None:
                            slot = item_instance.drop(ItemSlot(), [
                                client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                                client.account.character.pos[1] - 1,
                                client.account.character.pos[2] + r_math.randomFloat(-1, 1)])
                            world_items.append(slot)
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)
                        else:
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_inventory_slot_to_client(client, slotNum)
                    else:
                        server_log.entry("Roll: %f = No loot." % lootChance)

                if skill_name == "Mining":
                    lootChance = r_math.randomFloat(0, 9)

                    if lootChance < 1:
                        server_log.entry("Roll: %f = Get loot." % lootChance)


                        item_instance = None
                        gain_amount = 0

                        if context_name == "copper_ore":
                            server_log.entry("Copper")
                            item_instance = item_dictionary[ItemDictionaryIndex.CopperOre].generate()
                            gain_amount = 10
                        if context_name == "iron_ore":
                            server_log.entry("Iron")
                            item_instance = item_dictionary[ItemDictionaryIndex.IronOre].generate()
                            gain_amount = 25
                        if context_name == "nickel_ore":
                            server_log.entry("Nickel")
                            item_instance = item_dictionary[ItemDictionaryIndex.NickelOre].generate()
                            gain_amount = 42


                        if item_instance is None:
                            server_log.entry("Context not recognized.")
                            return

                        slotNum = client.account.character.inventory.addItem(item_instance)

                        client.account.character.skills["mining"].gain(gain_amount)
                        client.send(client.account.character.skills["mining"].state())

                        response = "{\"reward\":{\"skill\":\"Mining\",\"reward\":{\"xp\":%d,\"item\":\"Ore\"}}}" % gain_amount

                        if slotNum is None:
                            slot = ItemSlot(item_instance.base)
                            slot.add_to_stack(item_instance)
                            slot.world_position = [client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                                                   client.account.character.pos[1] - 1,
                                                   client.account.character.pos[2] + r_math.randomFloat(-1, 1)]
                            world_items.append(slot)
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)
                        else:
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_inventory_slot_to_client(client, slotNum)
                    else:
                        server_log.entry("Roll: %f = No loot." % lootChance)

                if skill_name == "Fishing":
                    lootChance = r_math.randomFloat(0, 9)

                    if lootChance < 1:
                        server_log.entry("Roll: %f = Get loot." % lootChance)


                        item_instance = None
                        gain_amount = 0

                        if context_name == "Pond":

                            fishing_level = client.account.character.skills["fishing"].current

                            if fishing_level < 2:
                                item_instance = item_dictionary[ItemDictionaryIndex.RawSalmon].generate()
                                gain_amount = 10
                            elif fishing_level < 3:
                                fishChance = r_math.randomFloat(0, 9)
                                if fishChance == 0:
                                    item_instance = item_dictionary[ItemDictionaryIndex.RawTuna].generate()
                                    gain_amount = 25
                                else:
                                    item_instance = item_dictionary[ItemDictionaryIndex.RawSalmon].generate()
                                    gain_amount = 10
                            else:
                                bassChance = r_math.randomFloat(0, 9)
                                tunaChance = r_math.randomFloat(0, 9)

                                if bassChance == 0:
                                    item_instance = item_dictionary[ItemDictionaryIndex.RawBass].generate()
                                    gain_amount = 42
                                elif tunaChance <= 5:
                                    item_instance = item_dictionary[ItemDictionaryIndex.RawTuna].generate()
                                    gain_amount = 25
                                else:
                                    item_instance = item_dictionary[ItemDictionaryIndex.RawSalmon].generate()
                                    gain_amount = 10


                        if item_instance is None:
                            server_log.entry("Context not recognized.")
                            return

                        slotNum = client.account.character.inventory.addItem(item_instance)

                        client.account.character.skills["fishing"].gain(gain_amount)
                        client.send(client.account.character.skills["fishing"].state())
                        response = "{\"reward\":{\"skill\":\"Fishing\",\"reward\":{\"xp\":%d,\"item\":\"Fish\"}}}" % gain_amount

                        if slotNum is None:
                            slot = ItemSlot(item_instance.base)
                            slot.add_to_stack(item_instance)
                            slot.world_position = [client.account.character.pos[0] + r_math.randomFloat(-1, 1),
                                                   client.account.character.pos[1] - 1,
                                                   client.account.character.pos[2] + r_math.randomFloat(-1, 1)]
                            world_items.append(slot)
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_drop_to_clients(server.clients, slot, len(world_items) - 1)
                        else:
                            server_log.entry(response)
                            client.send(response)
                            protocol.state_inventory_slot_to_client(client, slotNum)
                    else:
                        server_log.entry("Roll: %f = No loot." % lootChance)

                if skill_name == "Gathering":
                    response = "{\"reward\":{\"skill\":\"Gathering\",\"reward\":"
                    item_instance = None


                    world_id = int(context_name)

                    for w in world_items:
                        if w.world_id == world_id:
                            server_log.entry("Found item in world items dictionary")

                            if w.base is not None:

                                response += "{\"xp\":7,\"item\":\"" + w.base.name + "\"}"

                                client.account.character.skills["gathering"].gain(7)
                                client.send(client.account.character.skills["gathering"].state())


                                instance = w.take_from_stack()

                                if instance is not None:
                                    inventory_slot_num = client.account.character.inventory.addItem(instance)

                                    if inventory_slot_num is not None:
                                        response += "}}"

                                        if w.spawned:
                                            client.send(response)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)
                                    else:
                                        w.add_to_stack(instance)

                            break

                if skill_name == "Blacksmithing":

                    server_log.entry("Blacksmith context: " + context_name)

                    lootChance = r_math.randomFloat(0, 9)

                    if lootChance < 1:
                        server_log.entry("Roll: %f = Get loot." % lootChance)

                        response = "{\"reward\":{\"skill\":\"Blacksmithing\",\"reward\":"
                        item_slot = None

                        if context_name == "0":
                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Copper Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Copper Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.CuSword].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":13,\"item\":\"Cu Sword\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(13)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "12":
                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Iron Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Iron Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.FeSword].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":25,\"item\":\"Fe Sword\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(25)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "24":
                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Nickel Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Nickel Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.NiSword].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":38,\"item\":\"Ni Sword\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(38)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                        if context_name == "1":
                            server_log.entry("Cu Shield")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Copper Ore", 3)
                            if ore_slots is not None:
                                if len(ore_slots) == 3:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Copper Ore", 3)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CuShield].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":38,\"item\":\"Cu Shield\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(38)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "13":
                            server_log.entry("Fe Shield")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Iron Ore", 3)
                            if ore_slots is not None:
                                if len(ore_slots) == 3:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Iron Ore", 3)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.FeShield].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":75,\"item\":\"Fe Shield\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(75)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "25":
                            server_log.entry("Ni Shield")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Nickel Ore", 3)
                            if ore_slots is not None:
                                if len(ore_slots) == 3:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Nickel Ore", 3)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.NiShield].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":113,\"item\":\"Ni Shield\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(113)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                        if context_name == "2":
                            server_log.entry("Cu Arrow")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Copper Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Copper Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuArrow].generate())
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuArrow].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":13,\"item\":\"Cu Arrow x5\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(13)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "14":
                            server_log.entry("Fe Arrow")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Iron Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Iron Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeArrow].generate())
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeArrow].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":25,\"item\":\"Fe Arrow x5\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(25)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "26":
                            server_log.entry("Ni Arrow")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Nickel Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Nickel Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiArrow].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiArrow].generate())
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiArrow].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":38,\"item\":\"Ni Arrow x5\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(38)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                        if context_name == "3":
                            server_log.entry("Cu Axe")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Copper Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Copper Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.CuAxe].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":13,\"item\":\"Cu Axe\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(13)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "15":
                            server_log.entry("Fe Axe")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Iron Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Iron Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.FeAxe].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":25,\"item\":\"Fe Axe\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(25)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "27":
                            server_log.entry("Ni Axe")

                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Nickel Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Nickel Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.NiAxe].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":38,\"item\":\"Ni Axe\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(38)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)





                        if context_name == "4":
                            server_log.entry("Cu Pick")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Copper Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Copper Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CuPick].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":25,\"item\":\"Cu Pick\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(25)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "16":
                            server_log.entry("Fe Pick")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Iron Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Iron Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.FePick].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":50,\"item\":\"Fe Pick\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(50)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        if context_name == "28":
                            server_log.entry("Ni Pick")

                            server_log.entry("Found proper context")
                            ore_slots = client.account.character.inventory.hasItem("Nickel Ore", 2)
                            if ore_slots is not None:
                                if len(ore_slots) == 2:
                                    server_log.entry("Client has proper ore")
                                    used_items = client.account.character.inventory.removeItem("Nickel Ore", 2)
                                    if used_items is not None:
                                        server_log.entry("Collected ore")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.NiPick].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":75,\"item\":\"Ni Pick\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["blacksmithing"].gain(75)
                                        client.send(client.account.character.skills["blacksmithing"].state())

                                        for s in ore_slots:
                                            protocol.state_inventory_slot_to_client(client, s)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                        if context_name == "5":
                            server_log.entry("Found proper context")
                            ore_slot = client.account.character.inventory.hasItem("Copper Ore")
                            if ore_slot is not None:
                                server_log.entry("Client has proper ore")
                                used_item = client.account.character.inventory.removeItem("Copper Ore")
                                if used_item is not None:
                                    server_log.entry("Collected ore")
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.Copper].generate())
                                    client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.Copper].generate())
                                    inventory_slot_num = client.account.character.inventory.addItem(
                                        item_dictionary[ItemDictionaryIndex.Copper].generate())

                                    server_log.entry("Gave proper reward.")
                                    response += "{\"xp\":13,\"item\":\"Copper x3\"}"
                                    response += "}}"
                                    client.send(response)

                                    client.account.character.skills["blacksmithing"].gain(13)
                                    client.send(client.account.character.skills["blacksmithing"].state())

                                    protocol.state_inventory_slot_to_client(client, ore_slot)

                                    protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                    else:
                        server_log.entry("Roll: %f = No loot." % lootChance)

                if skill_name == "Crafting":

                    server_log.entry("Crafting context: " + context_name)

                    if ',' in context_name:
                        contexts = context_name.split(",")

                        use_item = client.account.character.inventory.slots[int(contexts[0])]
                        on_item = client.account.character.inventory.slots[int(contexts[1])]

                        server_log.entry("Use %s on %s" % (use_item.base.name, on_item.base.name))

                        use_slot = client.account.character.inventory.hasItem(use_item.base.name)
                        on_slot = client.account.character.inventory.hasItem(on_item.base.name)

                        use_instance = None
                        on_instance = None
                        assemble_slot = None

                        gain_amount = 0

                        while assemble_slot is None:
                            if "Sword" in use_item.base.name or "Sword" in on_item.base.name:
                                if "Hilt" in use_item.base.name or "Hilt" in on_item.base.name:
                                    if "Cu " in use_item.base.name or "Cu " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsCuSword].generate())
                                            gain_amount = 12
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeCuSword].generate())
                                            gain_amount = 19
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaCuSword].generate())
                                            gain_amount = 28
                                            break
                                    if "Fe " in use_item.base.name or "Fe " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFeSword].generate())
                                            gain_amount = 18
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFeSword].generate())
                                            gain_amount = 25
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFeSword].generate())
                                            gain_amount = 34
                                            break
                                    if "Ni " in use_item.base.name or "Ni " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsNiSword].generate())
                                            gain_amount = 24
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeNiSword].generate())
                                            gain_amount = 32
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaNiSword].generate())
                                            gain_amount = 40
                                            break

                            if "Shield" in use_item.base.name or "Shield" in on_item.base.name:
                                if "Frame" in use_item.base.name or "Frame" in on_item.base.name:
                                    if "Cu " in use_item.base.name or "Cu " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsCuShield].generate())
                                            gain_amount = 25
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeCuShield].generate())
                                            gain_amount = 35
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaCuShield].generate())
                                            gain_amount = 41
                                            break
                                    if "Fe " in use_item.base.name or "Fe " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFeShield].generate())
                                            gain_amount = 44
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFeShield].generate())
                                            gain_amount = 54
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFeShield].generate())
                                            gain_amount = 60
                                            break
                                    if "Ni " in use_item.base.name or "Ni " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsNiShield].generate())
                                            gain_amount = 63
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeNiShield].generate())
                                            gain_amount = 73
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaNiShield].generate())
                                            gain_amount = 79
                                            break

                            if "Arrow" in use_item.base.name or "Arrow" in on_item.base.name:
                                if "Shaft" in use_item.base.name or "Shaft" in on_item.base.name:
                                    if "Cu " in use_item.base.name or "Cu " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsCuArrow].generate())
                                            gain_amount = 9
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeCuArrow].generate())
                                            gain_amount = 15
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaCuArrow].generate())
                                            gain_amount = 24
                                            break
                                    if "Fe " in use_item.base.name or "Fe " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFeArrow].generate())
                                            gain_amount = 15
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFeArrow].generate())
                                            gain_amount = 21
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFeArrow].generate())
                                            gain_amount = 30
                                            break
                                    if "Ni " in use_item.base.name or "Ni " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsNiArrow].generate())
                                            gain_amount = 22
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeNiArrow].generate())
                                            gain_amount = 28
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaNiArrow].generate())
                                            gain_amount = 36
                                            break

                            if "Axe" in use_item.base.name or "Axe" in on_item.base.name:
                                if "Handle" in use_item.base.name or "Handle" in on_item.base.name:
                                    if "Cu " in use_item.base.name or "Cu " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsCuAxe].generate())
                                            gain_amount = 10
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeCuAxe].generate())
                                            gain_amount = 23
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaCuAxe].generate())
                                            gain_amount = 18
                                            break
                                    if "Fe " in use_item.base.name or "Fe " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFeAxe].generate())
                                            gain_amount = 16
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFeAxe].generate())
                                            gain_amount = 21
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFeAxe].generate())
                                            gain_amount = 24
                                            break
                                    if "Ni " in use_item.base.name or "Ni " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsNiAxe].generate())
                                            gain_amount = 22
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeNiAxe].generate())
                                            gain_amount = 27
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaNiAxe].generate())
                                            gain_amount = 30
                                            break

                            if "Pick" in use_item.base.name or "Pick" in on_item.base.name:
                                if "Handle" in use_item.base.name or "Handle" in on_item.base.name:
                                    if "Cu " in use_item.base.name or "Cu " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsCuPick].generate())
                                            gain_amount = 16
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeCuPick].generate())
                                            gain_amount = 21
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaCuPick].generate())
                                            gain_amount = 24
                                            break
                                    if "Fe " in use_item.base.name or "Fe " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFePick].generate())
                                            gain_amount = 28
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFePick].generate())
                                            gain_amount = 33
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFePick].generate())
                                            gain_amount = 36
                                            break
                                    if "Ni " in use_item.base.name or "Ni " in on_item.base.name:
                                        if "As " in use_item.base.name or "As " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsNiPick].generate())
                                            gain_amount = 41
                                            break
                                        if "Ce " in use_item.base.name or "Ce " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeNiPick].generate())
                                            gain_amount = 46
                                            break
                                        if "Ma " in use_item.base.name or "Ma " in on_item.base.name:
                                            use_instance = client.account.character.inventory.removeItem(use_item.base.name)
                                            on_instance = client.account.character.inventory.removeItem(on_item.base.name)
                                            assemble_slot = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaNiPick].generate())
                                            gain_amount = 49
                                            break


                            break

                        if assemble_slot is not None:
                            protocol.state_inventory_slot_to_client(client, use_slot)
                            protocol.state_inventory_slot_to_client(client, on_slot)
                            protocol.state_inventory_slot_to_client(client, assemble_slot)

                            client.account.character.skills["crafting"].gain(gain_amount)
                            client.send(client.account.character.skills["crafting"].state())

                            response = "{\"reward\":{\"skill\":\"Crafting\",\"reward\":"
                            response += "{\"xp\":%d,\"item\":\"%s\"}" % (gain_amount, assemble_slot.base.name)
                            response += "}}"

                            client.send(response)
                        else:
                            client.account.character.inventory.addItem(use_instance)
                            client.account.character.inventory.addItem(on_instance)


                    else:

                        lootChance = r_math.randomFloat(0, 9)

                        if lootChance < 1:

                            server_log.entry("Roll: %f = Get loot." % lootChance)

                            response = "{\"reward\":{\"skill\":\"Crafting\",\"reward\":"
                            item_slot = None


                            if context_name == "0":
                                server_log.entry("As Hilt")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Aspen Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Aspen Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsHilt].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":10,\"item\":\"As Hilt\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(10)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "1":
                                server_log.entry("As Frame")

                                server_log.entry("Found proper context")
                                log_slots = client.account.character.inventory.hasItem("Aspen Log", 2)
                                if log_slots is not None:
                                    if len(log_slots) == 2:
                                        server_log.entry("Client has proper log")
                                        used_items = client.account.character.inventory.removeItem("Aspen Log", 2)
                                        if used_items is not None:
                                            server_log.entry("Collected log")
                                            inventory_slot_num = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.AsFrame].generate())

                                            server_log.entry("Gave proper reward.")
                                            response += "{\"xp\":12,\"item\":\"As Frame\"}"
                                            response += "}}"
                                            client.send(response)

                                            client.account.character.skills["crafting"].gain(12)
                                            client.send(client.account.character.skills["crafting"].state())

                                            for s in log_slots:
                                                protocol.state_inventory_slot_to_client(client, s)

                                            protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "2":
                                server_log.entry("As Shaft")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Aspen Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Aspen Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsShaft].generate())
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsShaft].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":5,\"item\":\"As Shaft x4\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(5)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "3":
                                server_log.entry("As Handle")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Aspen Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Aspen Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.AsHandle].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":6,\"item\":\"As Handle\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(6)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                            if context_name == "12":
                                server_log.entry("Ce Hilt")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Cedar Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Cedar Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeHilt].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":25,\"item\":\"Ce Hilt\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(25)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "13":
                                server_log.entry("Ce Frame")

                                server_log.entry("Found proper context")
                                log_slots = client.account.character.inventory.hasItem("Cedar Log", 2)
                                if log_slots is not None:
                                    if len(log_slots) == 2:
                                        server_log.entry("Client has proper log")
                                        used_items = client.account.character.inventory.removeItem("Cedar Log", 2)
                                        if used_items is not None:
                                            server_log.entry("Collected log")
                                            inventory_slot_num = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.CeFrame].generate())

                                            server_log.entry("Gave proper reward.")
                                            response += "{\"xp\":32,\"item\":\"Ce Frame\"}"
                                            response += "}}"
                                            client.send(response)

                                            client.account.character.skills["crafting"].gain(32)
                                            client.send(client.account.character.skills["crafting"].state())

                                            for s in log_slots:
                                                protocol.state_inventory_slot_to_client(client, s)

                                            protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "14":
                                server_log.entry("Ce Shaft")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Cedar Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Cedar Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeShaft].generate())
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeShaft].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":17,\"item\":\"Ce Shaft x4\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(17)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "15":
                                server_log.entry("Ce Handle")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Cedar Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Cedar Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.CeHandle].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":16,\"item\":\"Ce Handle\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(16)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)



                            if context_name == "24":
                                server_log.entry("Ma Hilt")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Maple Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Maple Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaHilt].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":42,\"item\":\"Ma Hilt\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(42)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "25":
                                server_log.entry("Ma Frame")

                                server_log.entry("Found proper context")
                                log_slots = client.account.character.inventory.hasItem("Maple Log", 2)
                                if log_slots is not None:
                                    if len(log_slots) == 2:
                                        server_log.entry("Client has proper log")
                                        used_items = client.account.character.inventory.removeItem("Maple Log", 2)
                                        if used_items is not None:
                                            server_log.entry("Collected log")
                                            inventory_slot_num = client.account.character.inventory.addItem(
                                                item_dictionary[ItemDictionaryIndex.MaFrame].generate())

                                            server_log.entry("Gave proper reward.")
                                            response += "{\"xp\":44,\"item\":\"Ma Frame\"}"
                                            response += "}}"
                                            client.send(response)

                                            client.account.character.skills["crafting"].gain(44)
                                            client.send(client.account.character.skills["crafting"].state())

                                            for s in log_slots:
                                                protocol.state_inventory_slot_to_client(client, s)

                                            protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "26":
                                server_log.entry("Ma Shaft")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Maple Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Maple Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaShaft].generate())
                                        client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaShaft].generate())
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaShaft].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":34,\"item\":\"Ma Shaft x4\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(34)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)


                            if context_name == "27":
                                server_log.entry("Ma Handle")

                                server_log.entry("Found proper context")
                                log_slot = client.account.character.inventory.hasItem("Maple Log")
                                if log_slot is not None:
                                    server_log.entry("Client has proper log")
                                    used_item = client.account.character.inventory.removeItem("Maple Log")
                                    if used_item is not None:
                                        server_log.entry("Collected log")
                                        inventory_slot_num = client.account.character.inventory.addItem(
                                            item_dictionary[ItemDictionaryIndex.MaHandle].generate())

                                        server_log.entry("Gave proper reward.")
                                        response += "{\"xp\":22,\"item\":\"Ma Handle\"}"
                                        response += "}}"
                                        client.send(response)

                                        client.account.character.skills["crafting"].gain(22)
                                        client.send(client.account.character.skills["crafting"].state())

                                        protocol.state_inventory_slot_to_client(client, log_slot)

                                        protocol.state_inventory_slot_to_client(client, inventory_slot_num)

                        else:
                            server_log.entry("Roll %f = No Loot." % lootChance)

                if skill_name == "Attack":
                    char_id = int(context_name)

                    for c in npcs:
                        if c.id == char_id:
                            damage = 1

                            if client.account.character.equipped.slots[8].base is not None:
                                damage = client.account.character.equipped.slots[8].base.modifier

                            c.attack(damage)

                            response = "{\"reward\":{\"skill\":\"Attack\",\"reward\":{\"xp\":7,\"damage\":%d}}}" % damage

                            client.send(response)

                            client.account.character.skills["attack"].gain(7)
                            client.send(client.account.character.skills["attack"].state())

                            server_log.entry("Attack %s [%d]" % (c.name, c.skills["health"].current))
                            break


server = r_network.TCPServer(server_domain, server_port, command_handler, server_log, master_domain, master_port)

server_log.entry("[GAME] Waiting on server to start...")
server.start()

spawned = item_dictionary[ItemDictionaryIndex.MapleLog].generate().drop(ItemSlot(None, True), [277.9, 271.96, 9.8])
world_items.append(spawned)
server_log.entry("Spawned " + spawned.base.name)

wait = 0
while not server.isAlive:
    wait += 1
    if wait >= 999999:
        server_log.entry("[GAME] Server never started")
        quit()

currentTime = r_spacetime.When().milli
lastTick = currentTime()
while server.isAlive:
    if server.master is not None:
        if r_spacetime.When().milli() - server.lastMasterPing >= 12000:
            server.log.entry("[STALE MASTER]", server.log.EntryType.WARN)
            server.master.send("{\"server\":{\"address\":\"" + server.ip + "\"}}")
            server.lastMasterPing = r_spacetime.When().milli()

    if currentTime() - lastTick >= 600:
        game_tick(server)
        lastTick = currentTime()

    sleep(0.1)

server.thread.thread.join()
quit()