import ryde
from ryde import r_network
from ryde import r_logging
from ryde import r_account
from ryde import r_spacetime
from ryde.r_account import r_character
from ryde import r_math
from ryde import r_game


test_account = r_account.Account("TestUser")
test_account.character = r_character.Character("testCharacter")
test_account.character.pos = [275.876, 265.8, 9.885021]

item = r_game.generate_new_item(r_game.ItemDictionaryIndex.Copper)
print "Item: %s Stackable: %s Stack: %d Value: %d" % (item.name,
                                                              item.stackable,
                                                              item.stack,
                                                              item.value)
item.addAmount(10)
print "Item: %s Stackable: %s Stack: %d Value: %d" % (item.name,
                                                              item.stackable,
                                                              item.stack,
                                                              item.value)
item.removeAmount(1)
print "Item: %s Stackable: %s Stack: %d Value: %d" % (item.name,
                                                              item.stackable,
                                                              item.stack,
                                                              item.value)
otherItem = item.dequeOne()
print "Item: %s Stackable: %s Stack: %d Value: %d" % (item.name,
                                                      item.stackable,
                                                      item.stack,
                                                      item.value)

print "OtherItem Item: %s Stackable: %s Stack: %d Value: %d" % (otherItem.name,
                                                                otherItem.stackable,
                                                                otherItem.stack,
                                                                otherItem.value)

print "Slot[0] Item: %s " % (test_account.character.inventory.slots[0])
test_account.character.inventory.addItem(item)

print "Final State:"
print "Item: %s Stackable: %s Stack: %d Value: %d" % (item.name,
                                                      item.stackable,
                                                      item.stack,
                                                      item.value)

print "OtherItem Item: %s Stackable: %s Stack: %d Value: %d" % (otherItem.name,
                                                                otherItem.stackable,
                                                                otherItem.stack,
                                                                otherItem.value)
print "Slot[0] Item: %s Stackable: %s Stack: %d Value: %d" % (test_account.character.inventory.slots[0].name,
                                                              test_account.character.inventory.slots[0].stackable,
                                                              test_account.character.inventory.slots[0].stack,
                                                              test_account.character.inventory.slots[0].value)