=============
Altered State
=============

Altered State is an MMORPG Adventure Game made by `RydeTec <http://www.rydetec.com>`_.

The client is currently in development for Windows, Mac OS and iOS. The server is in development for linux written in Python utilizing the `Ryde Framework <ryde/>`_.

Account Management, Data Access, Game Logins and Purchases will be handled by the `Master Server <master/>`_.

Game Mechanics, AI, World State, Player Input and Chat will be handled by the `Game Server <game/>`_.


.. _reStructuredText: http://docutils.sourceforge.net/docs/user/rst/quickref.html