The game will be influenced by Diablo, Realm of the Mad God and Runescape. Movement should be WASD and the mouse will be used for interaction and combat.

Features we would like to implement

- Top-down graphics
- Block based randomly generated world
- Items should have randomly generated attributes
- Skill trees
    - Resource Skills
    - Processing Skills
    - Combat Skills
- Player classes
- Quests
- Player Trade Economy
    - Peer-to-Peer
    - Exchange House
- Player Inventory and Bank
- Player Equipment
- PvP
- Clans