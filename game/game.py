
import json
import ryde
from ryde import r_network
from ryde import r_logging
from ryde import r_account
from ryde import r_spacetime
from ryde.r_account import r_character
from ryde import r_math
from ryde.r_game.inventory import *
from ryde.r_game.item import *
from time import sleep


server_domain = "192.168.0.13"#r_network.get_local_ip()
server_port = 49867

server_log = r_logging.Log("Altered State Game Server", True, False, False)
server_log.entry("[GAME] Starting Altered State Game Server")

test_account = r_account.Account("TestUser")
test_account.character = r_character.Character("testCharacter")
test_account.character.pos = [275.876, 265.8, 9.885021]


cached_accounts = []
live_accounts = []

server_list = ["as-dev.rydetec.com"]

world_items = []

def client_tick(client):
    server_log.entry("[TICK][" + client.account.oculusID + "] Starting tick...")

    # Process all on_tick alters
    for alter in client.account.takeAlters():
        command_handler(client, alter)


turnCount = 25
def game_tick(server):
    server_log.entry("[TICK] Starting game tick...")

    # Survey server clients
    for c in server.clients:
        if not c.isAlive:
            server_log.entry("[TICK] %s:%d dead on tick. Removing from server." % (c.ip, c.port))
            c.thread.thread.join()
            server.clients.remove(c)
        else:
            if c.account is not None:
                client_tick(c)
            else:
                server_log.entry("[TICK] %s:%d  has not logged in yet." % (c.ip, c.port))

    # Sanction altered state
    global turnCount
    if test_account.character is not None:
        if test_account.character.name == "testCharacter":
            pos = list(test_account.character.pos)
            if turnCount <= 0:
                pos[2] = pos[2] - 2.0

                if turnCount == -25:
                    turnCount = 25

            if turnCount > 0:
                pos[2] = pos[2] + 2.0

            turnCount -= 1

            test_account.character.setPosition(pos[0], pos[1], pos[2])


    # State to clients
    for c in server.clients:
        if c.account is not None:
            if c.account.character is not None:
                client_state_command = "{\"state\":{\"characters\":["
                #c.send("{\"state\":{\"characters\":[")

                client_state_command += "{\"name\":\"%s\",\"x\":\"%f\",\"y\":\"%f\",\"z\":\"%f\"}," % (test_account.character.name,
                                                                                      test_account.character.pos[0],
                                                                                      test_account.character.pos[1],
                                                                                      test_account.character.pos[2])
                #c.send("{\"name\":\"%s\",\"x\":\"%f\",\"y\":\"%f\",\"z\":\"%f\"}," % (test_account.character.name,
                                                                                      #test_account.character.pos[0],
                                                                                      #test_account.character.pos[1],
                                                                                      #test_account.character.pos[2]))

                lastComma = len(server.clients)
                for cc in server.clients:
                    client_state_command += "{\"name\":\"%s\",\"x\":\"%f\",\"y\":\"%f\",\"z\":\"%f\"}" % (cc.account.character.name,
                                                                                          cc.account.character.pos[0],
                                                                                          cc.account.character.pos[1],
                                                                                          cc.account.character.pos[2])
                    #c.send("{\"name\":\"%s\",\"x\":\"%f\",\"y\":\"%f\",\"z\":\"%f\"}" % (cc.account.character.name,
                                                                                          #cc.account.character.pos[0],
                                                                                          #cc.account.character.pos[1],
                                                                                          #cc.account.character.pos[2]))

                    lastComma -= 1
                    if lastComma > 0:
                        client_state_command += ","
                        #c.send(",")

                client_state_command += "]}}"
                #c.send("]}}")

                #print client_state_command
                #c.send(client_state_command)

                inventory_state_command = "{\"state\":{\"inventory\":["
                #c.send("{\"state\":{\"inventory\":[")

                lastComma = len(c.account.character.inventory.slots)
                for s in c.account.character.inventory.slots:
                    if s.base is None:
                        inventory_state_command += "{}"
                        #c.send("{}")
                    else:
                        inventory_state_command += "{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":\"%d\",\"price\":\"%d\"}" % (
                            s.base.name,
                            s.base.stackable,
                            s.quantity(),
                            s.base.value
                        )
                        #c.send("{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":\"%d\",\"price\":\"%d\"}" % (
                        #    s.base.name,
                        #    s.base.stackable,
                        #    s.quantity(),
                        #    s.base.value
                        #))

                    lastComma -= 1
                    if lastComma > 0:
                        inventory_state_command += ","
                        #c.send(",")

                inventory_state_command += "]}}"
                #c.send("]}}")

                #print inventory_state_command
                #c.send(inventory_state_command)



                world_state_command = "{\"state\":{\"world\":["
                #c.send("{\"state\":{\"world\":[")

                lastComma = len(world_items)
                for s in world_items:
                    if s.base is None:
                        world_state_command += "{}"
                        #c.send("{}")
                    else:
                        world_state_command += "{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":\"%d\",\"price\":\"%d\",\"position\":[\"%f\",\"%f\",\"%f\"]}" % (
                            s.base.name,
                            s.base.stackable,
                            s.quantity(),
                            s.base.value,
                            s.world_position[0],
                            s.world_position[1],
                            s.world_position[2]
                        )
                        #c.send("{\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":\"%d\",\"price\":\"%d\",\"position\":[\"%f\",\"%f\",\"%f\"]}" % (
                            #s.base.name,
                            #s.base.stackable,
                            #s.quantity(),
                            #s.base.value,
                            #s.world_position[0],
                            #s.world_position[1],
                            #s.world_position[2]
                        #))

                    lastComma -= 1
                    if lastComma > 0:
                        world_state_command += ","
                        #c.send(",")

                world_state_command += "]}}"
                #c.send("]}}")

                #print world_state_command
                #c.send(world_state_command)


def command_handler(client, command):

    if command.get("hello"):
        server_log.entry("[COMMAND] Hello command detected.")

        id = command["hello"]["oculusID"]
        account = None

        for a in cached_accounts:
            if a.oculusID == id:
                server_log.entry("[COMMAND] Cached Account")
                account = a

        if account is None:
            server_log.entry("[COMMAND] New Account")
            account = r_account.Account(id)

            cached_accounts.append(account)

        client.set_account(account)

        account.character = None

        response = "{\"title\":{\"servers\":["

        for s in server_list:
            response += "\"" + s + "\""

        response += "],\"characters\":["

        lastComma = len(account.characterList)
        for c in account.characterList:
            if c is not None:
                response += "\"" + c.name + "\""
                lastComma -= 1
                if lastComma > 0:
                    response += ","

        response += "]}}"

        server_log.entry("[COMMAND] " + response)
        client.send(response)

    if command.get("login"):
        server_log.entry("Login command detected.")

        login = command["login"]

        if login.get("character"):
            slot = int(login["character"]["slot"])

            client.account.character = client.account.characterList[slot]

            character_string = "{\"name\":\"%s\",\"x\":\"%f\",\"y\":\"%f\",\"z\":\"%f\"}" % (client.account.character.name,
                                                                                             client.account.character.pos[0],
                                                                                             client.account.character.pos[1],
                                                                                             client.account.character.pos[2])

            response = "{\"state\":{\"player\":" + character_string + "}}"

            server_log.entry(response)
            client.send(response)
            client.isAlive = False

    if command.get("alter"):
        server_log.entry("Alter command detected.")

        alter = command["alter"]

        if alter.get("new"):
            type = alter["new"]

            if type.get("character"):
                name = type["character"]
                slot = int(type["slot"])

                client.account.character = r_account.r_character.Character(name)
                client.account.characterList[slot] = client.account.character
                client.account.character.pos = [277.9, 271.96, 9.8]

                for ii in range(0, 1500):
                    client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Copper].generate())

                for ii in range(0, 750):
                    client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Silver].generate())

                for ii in range(0, 10):
                    client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Gold].generate())

                for ii in range(0, 1):
                    client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Platinum].generate())

                for ii in range(0, 5):
                    client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Log].generate())

                client.account.character.inventory.to_terminal()

                character_string = "{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f}" % (
                    client.account.character.name,
                    client.account.character.pos[0],
                    client.account.character.pos[1],
                    client.account.character.pos[2])

                response = "{\"state\":{\"player\":" + character_string + "}}"

                server_log.entry(response)
                client.send(response)
                client.isAlive = False

        if alter.get("character"):
            character = alter["character"]

            if character.get("position"):
                position = character["position"]

                x = float(position[0])
                y = float(position[1])
                z = float(position[2])

                client.account.character.setPosition(x, y, z)

        if alter.get("move"):
            print "Move command detected"
            move = alter["move"]

            if move.get("item"):
                print "moving item"
                item = move["item"]["name"]
                source = move["item"]["source"]
                target = None
                if move["item"].get("target"):
                    target = move["item"]["target"]
                    print "to a target"

                if source == "inventory":
                    if target is None:
                        dropped_item = client.account.character.inventory.removeItem(item)
                        if dropped_item is not None:
                            slot = ItemSlot(dropped_item.base)
                            slot.add_to_stack(dropped_item)
                            slot.world_position = [client.account.character.pos[0] + r_math.randomFloat(-1,1),
                                                   client.account.character.pos[1] - 1,
                                                   client.account.character.pos[2] + r_math.randomFloat(-1,1)]
                            world_items.append(slot)
                        else:
                            server_log.entry("Dropped Item NoneType", server_log.EntryType.WARN)

        if alter.get("skill"):
            print "Skill command detected"

            skill = alter["skill"]

            if skill.get("action"):
                print "skill action"

                skill_name = skill["action"]["name"]

                if skill_name == "Woodcutting":
                    # Calculate hit chance and return if hit or not
                    # Calculate if should get log and xp

                    response = "{\"verified\":{\"action\":{\"skill\":\"Woodcutting\",   \"didHit\":"

                    chance = r_math.randomFloat(0, 10)

                    if chance >= 1:
                        print "Should hit"
                        response += "\"true\""

                        lootChance = r_math.randomFloat(0, 9)

                        if lootChance < 1:
                            print "Should get log and XP"
                            response += ",\"reward\":{\"xp\":\"7\",\"item\":\"Log\"}"
                            client.account.character.inventory.addItem(item_dictionary[ItemDictionaryIndex.Log].generate())
                    else:
                        print "Should miss"
                        response += "\"false\""

                    response += "}}}"

                    server_log.entry(response)
                    client.send(response)





server = r_network.TCPServer(server_domain, server_port, command_handler, server_log)

server_log.entry("[GAME] Waiting on server to start...")
server.start()


wait = 0
while not server.isAlive:
    wait += 1
    if wait >= 999999:
        server_log.entry("[GAME] Server never started")
        quit()

currentTime = r_spacetime.When().milli
lastTick = currentTime()
while server.isAlive:

    if currentTime() - lastTick >= 600:
        game_tick(server)
        lastTick = currentTime()

    sleep(0.1)

server.thread.thread.join()
quit()