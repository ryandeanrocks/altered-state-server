
class Account:

    # An account object will be created for every unique oculus profile
    # It will be identified by a provided oculusID
    # It will hold the accounts active character object
    # It will cache all alters and states between the client and server
    # It will handle all server stored purchasing information

    def __init__(self, oculusID):
        self.oculusID = oculusID

        self.character = None
        self.characterList = [None, None, None, None]

        self.alters = []
        self.states = []

        self.drink = 0
        self.pizza = 0

    def set_character(self, slot, object):
        self.characterList[slot] = object

    # def setCharacter(self, character):
    #    self.character = character

    def add_alter(self, alter):
        self.alters.append(alter)

    def clear_alters(self):
        self.alters = []

    def clear_states(self):
        self.states = []

    def add_state(self, state):
        self.states.append(state)

    def add_drink(self):
        self.drink += 1

    def add_pizza(self):
        self.pizza += 1




