
from ryde import r_game
from ryde.r_game.inventory import *
from ryde.r_math import *
from ryde.r_spacetime import *
from ryde.r_game.skill import *
from ryde.r_game.quest import *

class Character:
    next_character_id = 0

    def __init__(self, name, json=None):
        self.role = 0

        self.inventory = Inventory("inventory", 21)
        self.bank = Inventory("bank", 60, True)
        self.equipped = Inventory("equipped", 11, False, True)

        self.lastDied = 0

        self.npc_id = 0

        self.needsUpdate = True
        self.aggro = None

        self.skills = {
            "woodcutting": Skill("Woodcutting", 1, 1, 0),
            "crafting": Skill("Crafting", 1, 1, 0),
            "archery": Skill("Archery", 1, 1, 0),
            "health": Skill("Health", 10, 10, 1154),
            "mining": Skill("Mining", 1, 1, 0),
            "blacksmithing": Skill("Blacksmithing", 1, 1, 0),
            "magic": Skill("Magic", 1, 1, 0),
            "attack": Skill("Attack", 1, 1, 0),
            "fishing": Skill("Fishing", 1, 1, 0),
            "cooking": Skill("Cooking", 1, 1, 0),
            "stealth": Skill("Stealth", 1, 1, 0),
            "strength": Skill("Strength", 1, 1, 0),
            "gathering": Skill("Gathering", 1, 1, 0),
            "farming": Skill("Farming", 1, 1, 0),
            "alchemy": Skill("Alchemy", 1, 1, 0),
            "defense": Skill("Defense", 1, 1, 0)
        }

        self.quests = [
            Quest("Tutorial", 1, 0)
        ]

        self.shouldSpawn = False

        if json is None:
            self.id = Character.next_character_id
            Character.next_character_id += 1

            self.name = name

            self.spawn = [0.0, 0.0, 0.0]
            self.pos = [0.0, 0.0, 0.0]

            self.lastPos = [0.0, 0.0, 0.0]

        else:
            self.id = json["player"]["character"]["id"]
            self.name = json["player"]["character"]["name"]
            self.spawn = [json["player"]["character"]["sx"], json["player"]["character"]["sy"], json["player"]["character"]["sz"]]
            self.pos = [json["player"]["character"]["x"], json["player"]["character"]["y"], json["player"]["character"]["z"]]

            for item in json["player"]["inventory"]:
                self.inventory.slots[int(item["slot"])] = ItemSlot()
                if item["name"] != "Empty":
                    for base in item_dictionary:
                        if item["name"] == item_dictionary[base].name:
                            for n in range(0, item["quantity"]):
                                self.inventory.slots[int(item["slot"])].add_to_stack(
                                    item_dictionary[base].generate())

                            break

            for item in json["player"]["bank"]:
                self.bank.slots[int(item["slot"])] = ItemSlot()
                if item["name"] != "Empty":
                    for base in item_dictionary:
                        if item["name"] == item_dictionary[base].name:
                            for n in range(0, item["quantity"]):
                                self.bank.slots[int(item["slot"])].add_to_stack(
                                    item_dictionary[base].generate())

                            break

            for item in json["player"]["equipped"]:
                self.equipped.slots[int(item["slot"])] = ItemSlot()
                if item["name"] != "Empty":
                    for base in item_dictionary:
                        if item["name"] == item_dictionary[base].name:
                            for n in range(0, item["quantity"]):
                                self.equipped.slots[int(item["slot"])].add_to_stack(
                                    item_dictionary[base].generate())

                            break

            self.lastPos = [json["player"]["character"]["x"], json["player"]["character"]["y"], json["player"]["character"]["z"]]

            for skill in self.skills:
                for s in json["player"]["skills"]:
                    if s.get(skill):
                        self.skills[skill].level = s[skill]["level"]
                        self.skills[skill].current = s[skill]["current"]
                        self.skills[skill].xp = s[skill]["xp"]
                        break

            for q in json["player"]["quests"]:
                for quest in self.quests:
                    if q.get(quest.name):
                        quest.step = q[quest.name]["step"]
                        quest.stage = q[quest.name]["stage"]
                        break

        # 0 = Player, 1 = Tester
        if self.name == "CoreyRyanDean-0":
            self.role = 1


    def setPosition(self, x, y, z):
        self.pos = [x, y, z]

    def attack(self, value):
        self.skills["health"].current -= value
        self.needsUpdate = True

        if self.skills["health"].current <= 0:
            self.lastDied = When().milli()

    def player_die(self):
        self.pos = [277.9, 271.96, 9.8]
        self.skills["health"].current = self.skills["health"].level

    def tick(self, clients):

        if self.shouldSpawn:
            self.pos = self.spawn
            self.skills["health"].current = self.skills["health"].level
            self.shouldSpawn = False
            self.aggro = None
            self.needsUpdate = True

        if self.skills["health"].current <= 0:
            self.skills["health"].current = 0

            if When().milli() - self.lastDied > 30000:
                self.shouldSpawn = True

            return

        if self.aggro is None:
            for c in clients:
                if c.account is not None:
                    if c.account.character is not None:
                        source = [self.pos[0],self.pos[2]]
                        target = [c.account.character.pos[0],c.account.character.pos[2]]

                        dist, dir = calculateDistanceDirection(source, target)

                        #print "Aggro distance: %f" % dist

                        if dist < 10:
                            self.aggro = c.account.character
                            break
        else:
            if self.aggro.skills["health"].current <= 0:
                self.aggro = None

        if self.aggro is None:

            self.update_ai()
        else:

            source = [self.pos[0],self.pos[2]]
            target = [self.aggro.pos[0], self.aggro.pos[2]]

            dist, dir = calculateDistanceDirection(source, target)

            #print "deaggro distance: %f" % dist

            if dist < 25:
                if dist > 3.0:
                    self.pos[0] = self.pos[0] - (dir[0] * 2.0)
                    self.pos[2] = self.pos[2] - (dir[1] * 2.0)
                else:
                    damage = 1
                    hitChance = 1

                    if self.aggro.equipped.slots[9].base is not None:
                        hitChance = randomFloat(0, self.aggro.equipped.slots[9].base.modifier)

                    if hitChance <= 1:
                        self.aggro.attack(1)

                    if self.aggro.skills["health"].current <= 0:
                        self.aggro = None
            else:
                self.aggro = None

        source = [self.lastPos[0],self.lastPos[2]]
        target = [self.pos[0],self.pos[2]]

        dist, dir = calculateDistanceDirection(source, target)

        #print "pos: %s lpos: %s dist: %f" % (self.pos, self.lastPos, dist)

        if dist >= 0.5:
            self.lastPos = list(self.pos)
            self.needsUpdate = True

            #print "pos: %s" % self.pos

    def update_ai(self):

        if self.npc_id == 0:
            minX = 240
            maxX = 268
            minZ = 105
            maxZ = 170

            walkChance = randomFloat(0, 10)

            if walkChance < 1.0:
                self.pos[0] = self.pos[0] + randomFloat(-5.0, 5.0)
                self.pos[2] = self.pos[2] + randomFloat(-5.0, 5.0)

            if self.pos[0] < minX:
                self.pos[0] = minX

            if self.pos[0] > maxX:
                self.pos[0] = maxX

            if self.pos[2] < minZ:
                self.pos[2] = minZ

            if self.pos[2] > maxZ:
                self.pos[2] = maxZ

            return