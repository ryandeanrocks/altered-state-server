import json

from ryde import r_thread
from ryde import r_spacetime

import select
import socket

class ServerClient:

    def __init__(self, conn, ip, port, handler, log):
        self.conn = conn
        self.ip = ip
        self.port = port
        self.log = log

        self.isServer = False

        self.isReady = True
        self.shouldStop = False
        self.isAlive = True
        self.account = None

        self.handler = handler

        self.lastMessage = r_spacetime.When().milli()

        self.partial = ""

    def set_account(self, account):
        self.account = account

    # def run(self):
    #     self.isAlive = True
    #
    #     timeout_timer = 0
    #
    #     while not self.shouldStop:
    #         self.isReady = True
    #         try:
    #             self.conn.settimeout(60)
    #
    #             try:
    #                 data = self.conn.recv(2048)
    #                 timeout_timer = 0
    #             except Exception as e:
    #                 data = False
    #                 if not self.isServer:
    #                     self.log.entry("[CLIENT] %s:%d will time out in %d" % (self.ip, self.port, (5 - timeout_timer)), self.log.EntryType.WARN)
    #                     timeout_timer += 1
    #                 else:
    #                     self.log.entry("[SERVER] %s:%d will time out in %d" % (self.ip, self.port, (5 - timeout_timer)),
    #                                    self.log.EntryType.WARN)
    #
    #                     #self.shouldStop = True
    #                     timeout_timer = 0
    #             try:
    #                 if not self.thread.main_alive():
    #                     self.log.entry("[CLIENT] Server dead, killing client")
    #                     self.shouldStop = True
    #             except Exception as e:
    #                 self.log.entry("Thread started to quick. Login missed. %s" % e)
    #                 continue
    #
    #             if timeout_timer >= 5:
    #                 self.log.entry("[CLIENT] %s:%d timed out." % (self.ip, self.port), self.log.EntryType.WARN)
    #                 self.shouldStop = True
    #
    #             if self.shouldStop:
    #                 self.log.entry("[CLIENT] %s:%d should stop!" % (self.ip, self.port))
    #                 break
    #
    #             if not data:
    #                 continue
    #
    #             self.lastMessage = r_spacetime.When().milli()
    #
    #             self.log.entry("[CLIENT] %s:%d sent: %s" % (self.ip, self.port, data))
    #
    #             datas = data.split("}{")
    #
    #             ind = 0
    #             if len(datas) > 1:
    #                 for d in datas:
    #                     if ind > 0:
    #                         datas[ind] = "{" + datas[ind]
    #
    #                     if ind < len(datas) - 1:
    #                         datas[ind] += "}"
    #                     ind += 1
    #
    #             for d in datas:
    #                 if d == "{\"logout\":0}":
    #                     self.shouldStop = True
    #                     break
    #
    #                 if self.account is not None:
    #                     self.account.addAlter(json.loads(d.decode("utf-8")))
    #                 else:
    #                     self.handler(self, json.loads(d.decode("utf-8")))
    #         except Exception as e:
    #             print "Crash: %s" % e
    #
    #     self.conn.close()
    #     self.isAlive = False
    #     self.log.entry("[CLIENT] %s:%d is dead." % (self.ip, self.port))

    def send(self, string, onTick=False):
        try:
            if self.isAlive and self.account is not None and not onTick:
                self.account.add_state(string)
            else:
                self.conn.send(string)
        except Exception as e:
            self.log.entry("[CLIENT] %s" % e, self.log.EntryType.ERROR)
            self.shouldStop = True


# class MasterClient:
#
#     def __init__(self, ip, port, handler, log):
#         self.ip = ip
#         self.port = port
#         self.handler = handler
#         self.log = log
#
#         self.isReady = False
#         self.shouldStop = False
#         self.isAlive = False
#
#         self.conn = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#
#         self.thread = r_thread.RyThread(self.run, [])
#
#     def run(self):
#         self.isAlive = True
#
#         self.conn.connect((self.ip, self.port))
#
#         timeout_timer = 0
#
#         self.send("{\"server\":{\"address\":\"" + self.ip + "\"}}")
#
#         partial = ""
#
#         while not self.shouldStop:
#             self.isReady = True
#             try:
#                 self.conn.settimeout(12)
#                 try:
#                     data = self.conn.recv(2048)
#                     timeout_timer = 0
#                 except Exception as e:
#                     data = False
#                     self.log.entry("[STALE MASTER]", self.log.EntryType.WARN)
#                     timeout_timer = 0
#                     #self.log.entry("[CLIENT] %s:%d will time out in %d" % (self.ip, self.port, (5 - timeout_timer)),
#                     self.send("{\"server\":{\"address\":\"" + self.ip + "\"}}")
#
#                     #timeout_timer += 1
#
#                 if not self.thread.main_alive():
#                     self.log.entry("[SERVER] Server dead, killing client")
#                     self.shouldStop = True
#
#                 if timeout_timer >= 5:
#                     self.log.entry("[SERVER] %s:%d timed out." % (self.ip, self.port), self.log.EntryType.WARN)
#                     self.shouldStop = True
#
#                 if not data:
#                     continue
#
#                 if self.shouldStop:
#                     self.log.entry("[SERVER] %s:%d should stop!" % (self.ip, self.port))
#                     break
#
#
#                 self.log.entry("[SERVER] %s:%d sent: %s" % (self.ip, self.port, data))
#
#                 partial += data
#
#                 opens = 0
#                 closes = 0
#
#                 for p in partial:
#                     if p == '{':
#                         opens += 1
#                     if p == '}':
#                         closes += 1
#
#                 if opens < closes:
#                     self.log.entry("TRASH DATA: %s" % partial)
#                     partial = ""
#                     continue
#
#                 if opens > closes:
#                     continue
#
#                 datas = partial.split("}{")
#
#                 ind = 0
#                 if len(datas) > 1:
#                     for d in datas:
#                         if ind > 0:
#                             datas[ind] = "{" + datas[ind]
#
#                         if ind < len(datas) - 1:
#                             datas[ind] += "}"
#                         ind += 1
#
#                 for d in datas:
#                     self.handler(self, json.loads(d.decode("utf-8")))
#             except Exception as e:
#                 print e
#
#         self.conn.close()
#         self.isAlive = False
#         self.log.entry("[SERVER] %s:%d is dead." % (self.ip, self.port))
#
#     def send(self, string):
#         try:
#             self.conn.send(string)
#         except Exception as e:
#             self.log.entry("[SERVER] %s" % e, self.log.EntryType.ERROR)


class TCPServer:

    def __init__(self, ip, port, handler, log, master_domain=None, master_port=None):
        self.ip = ip
        self.port = port
        self.buffer = 20

        self.isReady = False
        self.isAlive = False

        self.commands = []
        self.clients = []

        self.socket = None

        self.masterDomain = master_domain
        self.masterPort = master_port
        self.master = None
        self.masterPartial = ""
        self.lastMasterPing = 0

        self.log = log

        self.thread = None

        self.handler = handler

        self.inputs = []
        self.outputs = []

    def start(self):
        self.isAlive = True

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setblocking(0)

        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((self.ip, self.port))
        self.socket.settimeout(600)
        self.log.entry("[SERVER] Server was started at %s:%d" % (self.ip, self.port))

        self.inputs = [self.socket]
        self.socket.listen(5)

        if self.masterDomain is not None:
            self.master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

            self.master.connect((self.masterDomain, self.masterPort))
            self.master.send("{\"server\":{\"address\":\"" + self.ip + "\"}}")
            self.master.settimeout(12)

            self.master.setblocking(0)
            self.inputs.append(self.master)

        self.thread = r_thread.RyThread(self.update, [])

    def update(self):
        self.isReady = True
        while self.isAlive:

            for c in self.clients:
                if not c.isAlive:
                    self.clients.remove(c)
                    continue

            #print len(self.clients)

            readable, writable, exceptional = select.select(self.inputs, self.outputs, self.inputs)

            for s in readable:

                if self.master is not None:
                    if s is self.master:
                        try:
                            data = s.recv(2048)
                        except Exception as e:
                            data = False
                            self.log.entry("[STALE MASTER]", self.log.EntryType.WARN)
                            self.master.send("{\"server\":{\"address\":\"" + self.ip + "\"}}")

                        if not data:
                            self.log.entry("DEAD MASTER!!!!!!")
                            continue

                        self.log.entry("[SERVER] %s:%d sent: %s" % (self.ip, self.port, data))

                        self.masterPartial += data

                        opens = 0
                        closes = 0

                        for p in self.masterPartial:
                            if p == '{':
                                opens += 1
                            if p == '}':
                                closes += 1

                        if opens < closes:
                            self.log.entry("TRASH DATA: %s" % self.masterPartial)
                            self.masterPartial = ""
                            continue

                        if opens > closes:
                            continue

                        datas = self.masterPartial.split("}{")

                        self.masterPartial = ""

                        ind = 0
                        if len(datas) > 1:
                            for d in datas:
                                if ind > 0:
                                    datas[ind] = "{" + datas[ind]

                                if ind < len(datas) - 1:
                                    datas[ind] += "}"
                                ind += 1

                        for d in datas:
                            self.handler(self, json.loads(d.decode("utf-8")))

                        continue

                if s is self.socket:

                    try:
                        (conn, (ip, port)) = self.socket.accept()

                        newClient = ServerClient(conn, ip, port, self.handler, self.log)
                        self.inputs.append(newClient.conn)
                        while not newClient.isReady:
                            self.log.entry("Waiting on %s:%d to connect..." % (ip, port))

                        self.clients.append(newClient)
                        self.log.entry("Client connected")

                    except Exception as e:
                        self.log.entry("[SERVER] Client listener timeout")
                else:
                    for c in self.clients:
                        if c.conn is s:
                            try:
                                data = s.recv(2048)
                                c.timeout_timer = 0
                            except Exception as e:
                                data = False
                                if not c.isServer:
                                    self.log.entry(
                                        "[CLIENT] %s:%d will time out in %d" % (c.ip, c.port, (5 - c.timeout_timer)),
                                        self.log.EntryType.WARN)
                                    c.timeout_timer += 1
                                else:
                                    self.log.entry(
                                        "[SERVER] %s:%d will time out in %d" % (c.ip, c.port, (5 - c.timeout_timer)),
                                        self.log.EntryType.WARN)

                                    c.timeout_timer = 0

                            if c.timeout_timer >= 5:
                                self.log.entry("[CLIENT] %s:%d timed out." % (c.ip, c.port),
                                               self.log.EntryType.WARN)
                                c.shouldStop = True

                            if not data:
                                c.shouldStop = True
                            else:

                                c.lastMessage = r_spacetime.When().milli()

                                self.log.entry("[CLIENT] %s:%d sent: %s" % (c.ip, c.port, data))

                                c.partial += data

                                opens = 0
                                closes = 0

                                for p in c.partial:
                                    if p == '{':
                                        opens += 1
                                    if p == '}':
                                        closes += 1

                                if opens < closes:
                                    self.log.entry("TRASH DATA: %s" % c.partial)
                                    c.partial = ""
                                    continue

                                if opens > closes:
                                    continue

                                data = c.partial
                                c.partial = ""

                                print data

                                datas = data.split("}{")

                                ind = 0
                                if len(datas) > 1:
                                    for d in datas:
                                        if ind > 0:
                                            datas[ind] = "{" + datas[ind]

                                        if ind < len(datas) - 1:
                                            datas[ind] += "}"
                                        ind += 1

                                for d in datas:
                                    if d == "{\"logout\":0}":
                                        c.shouldStop = True

                                    if c.account is not None:
                                        c.account.add_alter(json.loads(d.decode("utf-8")))
                                    else:
                                        c.handler(c, json.loads(d.decode("utf-8")))

                            if c.shouldStop:
                                self.log.entry("[CLIENT] %s:%d should stop!" % (c.ip, c.port))

                                c.conn.close()
                                c.isAlive = False
                                self.inputs.remove(c.conn)
                                self.clients.remove(c)


                            break

            if not self.thread.main_alive():
                for c in self.clients:
                    c.shouldStop = True

                self.isAlive = False
                break

        self.log.entry("[SERVER] Shutting down...")


def get_local_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    address = s.getsockname()[0]
    s.close()
    return address
