
from ryde.r_math import *

def state_full_world_sync(client):
    client.send("{\"state\":{\"world\":{}}}")

def state_full_sync_complete(client):
    client.send("{\"state\":{\"world\":{\"status\":\"complete\"}}}")

def state_world_to_client(client, world_items):
    for i, slot in enumerate(world_items):
        item_string = "{\"state\":{\"world\":{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d,\"world_id\":%d,\"position\":[%f,%f,%f]}}}" % (
            i,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value,
            slot.world_id,
            slot.world_position[0],
            slot.world_position[1],
            slot.world_position[2]
        )
        client.send(item_string)

def state_drop_to_clients(clients, slot, index):
    for c in clients:
        item_string = "{\"state\":{\"world\":{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d,\"world_id\":%d,\"position\":[%f,%f,%f]}}}" % (
            index,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value,
            slot.world_id,
            slot.world_position[0],
            slot.world_position[1],
            slot.world_position[2]
        )
        c.send(item_string)

def state_npcs_to_client(client, npcs, initial=False):
    for n in npcs:
        if client.account is not None:
            if client.account.character is not None:
                source = [client.account.character.pos[0], client.account.character.pos[2]]
                target = [n.pos[0], n.pos[2]]

                dist, dir = calculateDistanceDirection(source, target)

                #print "From player %f" % dist

                if dist < 125:
                    npc_string = "{\"state\":{\"npc\":{\"id\":%d,\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f,\"health\":%d}}}" % (
                                                n.id,
                                                n.name,
                                                n.pos[0],
                                                n.pos[1],
                                                n.pos[2],
                                                n.skills["health"].current)

                    if initial or n.needsUpdate:
                        client.send(npc_string)

def state_clients_to_client(client, clients, initial=False):
    for c in clients:
        if client.account is not None and c.account is not None:
            if client.account.character is not None and c.account.character is not None:
                if client is not c:
                    source = [client.account.character.pos[0], client.account.character.pos[2]]
                    target = [c.account.character.pos[0], c.account.character.pos[2]]

                    dist, dir = calculateDistanceDirection(source, target)

                    # print "From player %f" % dist

                    if dist < 125:
                        char_string = "{\"state\":{\"character\":{\"id\":%d,\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f,\"health\":%d,\"role\":%d}}}" % (
                            c.account.character.id,
                            c.account.character.name,
                            c.account.character.pos[0],
                            c.account.character.pos[1],
                            c.account.character.pos[2],
                            c.account.character.skills["health"].current,
                            c.account.character.role)

                        print char_string

                        client.send(char_string)

def state_client_to_master(master, client):
    char = client.account.character

    master.send("{\"server\":{\"state\":{\"oculus\":\"" + client.account.oculusID + "\",\"player\":")

    # Send all persistent character data to master server
    master.send("{\"character\":{\"id\":%d,\"name\":\"%s\"," % (char.id, char.name))

    # Position
    master.send("\"x\":%f,\"y\":%f,\"z\":%f," % (char.pos[0], char.pos[1], char.pos[2]))
    # Spawn
    master.send("\"sx\":%f,\"sy\":%f,\"sz\":%f}," % (char.spawn[0], char.spawn[1], char.spawn[2]))
    # Inventory
    master.send("\"inventory\":[")
    for num, slot in enumerate(char.inventory.slots):
        if slot.base is None:
            master.send("{\"slot\":%d,\"name\":\"Empty\"}" % num)
            if char.inventory.slots[-1] != slot:
                master.send(",")
        else:
            master.send("{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                num,
                slot.base.name,
                slot.base.stackable,
                slot.quantity()
            ))
            if char.inventory.slots[-1] != slot:
                master.send(",")
    master.send("],")
    # Bank
    master.send("\"bank\":[")
    for num, slot in enumerate(char.bank.slots):
        if slot.base is None:
            master.send("{\"slot\":%d,\"name\":\"Empty\"}" % num)
            if char.bank.slots[-1] != slot:
                master.send(",")
        else:
            master.send("{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                num,
                slot.base.name,
                slot.base.stackable,
                slot.quantity()
            ))
            if char.bank.slots[-1] != slot:
                master.send(",")
    master.send("],")
    # Equipped
    master.send("\"equipped\":[")
    for num, slot in enumerate(char.equipped.slots):
        if slot.base is None:
            master.send("{\"slot\":%d,\"name\":\"Empty\"}" % num)
            if char.equipped.slots[-1] != slot:
                master.send(",")
        else:
            master.send("{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d}" % (
                num,
                slot.base.name,
                slot.base.stackable,
                slot.quantity()
            ))
            if char.equipped.slots[-1] != slot:
                master.send(",")
    master.send("],")
    # Skills
    master.send("\"skills\":[")
    ii = 0
    for name in char.skills:
        master.send("{\"%s\":{\"level\":%d,\"current\":%d,\"xp\":%d}}" % (name, client.account.character.skills[name].level, client.account.character.skills[name].current, client.account.character.skills[name].xp))
        if ii != len(char.skills) - 1:
            master.send(",")
        ii += 1
    master.send("],")
    # Quests
    master.send("\"quests\":[")
    for quest in char.quests:
        master.send("{\"%s\":{\"stage\":%d,\"step\":%d}}" % (quest.name, quest.stage, quest.step))
        if char.quests[-1] != quest:
            master.send(",")
    master.send("]")


    master.send("}}}}")


def state_quests_to_client(client):
    for q in client.account.character.quests:
        client.send(q.state())

def state_store_to_client(client, store):
    for i, s in enumerate(store.inventory.slots):
        state_store_slot_to_client(client, store, i)

def state_store_slot_to_client(client, store, num):
    inventory_string = "{\"state\":{\"store\":"

    slot = store.inventory.slots[num]

    if slot.base is None:
        client.send(inventory_string + ("{\"slot\":%d,\"name\":\"Empty\"}}}" % num))
    else:
        item_string = "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}}}" % (
            num,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value
        )

        client.send(inventory_string + item_string)

def state_skill_to_client(client, skill):
    client.send(client.account.character.skills[skill].state())

def state_skills_to_client(client):
    for s in client.account.character.skills:
        client.send(client.account.character.skills[s].state())
    #skill_string = "{\"state\":{\"skill\":{\"name\":\"Health\",\"value\":%d,\"max\":%d}}}" % (client.account.character.health, client.account.character.maxHealth)
    #client.send(skill_string)

def state_inventory_to_client(client):
    for i, s in enumerate(client.account.character.inventory.slots):
        state_inventory_slot_to_client(client, i)


def state_inventory_slot_to_client(client, num):
    inventory_string = "{\"state\":{\"inventory\":"

    slot = client.account.character.inventory.slots[num]

    if slot.base is None:
        client.send(inventory_string + ("{\"slot\":%d,\"name\":\"Empty\"}}}" % num))
    else:
        item_string = "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}}}" % (
            num,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value
        )

        client.send(inventory_string + item_string)


def state_bank_to_client(client):
    for i, s in enumerate(client.account.character.bank.slots):
        state_bank_slot_to_client(client, i)


def state_bank_slot_to_client(client, num):
    inventory_string = "{\"state\":{\"bank\":"

    slot = client.account.character.bank.slots[num]

    if slot.base is None:
        client.send(inventory_string + ("{\"slot\":%d,\"name\":\"Empty\"}}}" % num))
    else:
        item_string = "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}}}" % (
            num,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value
        )

        client.send(inventory_string + item_string)


def state_equipped_to_client(client):
    for i, s in enumerate(client.account.character.equipped.slots):
        state_equipped_slot_to_client(client, i)


def state_equipped_slot_to_client(client, num):
    inventory_string = "{\"state\":{\"equipped\":"

    slot = client.account.character.equipped.slots[num]

    if slot.base is None:
        client.send(inventory_string + ("{\"slot\":%d,\"name\":\"Empty\"}}}" % num))
    else:
        item_string = "{\"slot\":%d,\"name\":\"%s\",\"stackable\":\"%s\",\"quantity\":%d,\"price\":%d}}}" % (
            num,
            slot.base.name,
            slot.base.stackable,
            slot.quantity(),
            slot.base.value
        )

        print inventory_string + item_string
        client.send(inventory_string + item_string)


def state_player_to_client(client):
    character_string = "{\"state\":{\"player\":{\"name\":\"%s\",\"x\":%f,\"y\":%f,\"z\":%f,\"role\":%d}}}" % (
        client.account.character.name,
        client.account.character.pos[0],
        client.account.character.pos[1],
        client.account.character.pos[2],
        client.account.character.role)

    client.send(character_string)
    print character_string
