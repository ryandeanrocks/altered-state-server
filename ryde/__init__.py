import collections
from enum import Enum


def enum(*keys):
    return collections.namedtuple('Enum', keys)(*range(len(keys)))