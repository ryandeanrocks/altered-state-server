import math

class Skill:

    def __init__(self, name, current, level, xp):
        self.name = name
        self.current = current
        self.level = level
        self.xp = xp

    def xp_needed(self):
        nlxp = 0
        for i in range(1, self.level + 1):
            nlxp += (i + (300 * (pow(2, (i / 7.0)))))
        nlxp = math.floor(nlxp) / 4.0
        return int(math.floor(nlxp))

    def state(self):
        return "{\"state\":{\"skill\":{\"name\":\"%s\",\"current\":%d,\"level\":%d,\"xp\":%d,\"needed\":%d}}}" % (
            self.name, self.current, self.level, self.xp, self.xp_needed())

    def gain(self, val):
        self.xp += val

        self.levelUp()

    def levelUp(self):

        if self.xp >= self.xp_needed():
            self.level += 1
            self.current += 1

            if not self.levelUp():
                print "done leveling up"

            return True
        return False
