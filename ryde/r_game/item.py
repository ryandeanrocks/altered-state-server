from ryde import enum

class ItemInstance:

    def __init__(self, id, base):
        self.id = id
        self.base = base

    def drop(self, slot, position):
        slot.add_to_stack(self)
        slot.world_position = position
        return slot




class Item:
    maxStack = 1000000000

    def __init__(self, name, value=0, stackable=False, type=0, equip=-1, modifier=0):
        self.name = name
        self.value = value
        self.stackable = stackable

        self.lastId = 0
        self.type = type
        self.equip = equip
        self.modifier = modifier

    def generate(self):
        self.lastId += 1
        return ItemInstance(self.lastId, self)



item_dictionary = {}

ItemDictionaryIndex = enum(
    'Copper',
    'Silver',
    'Gold',
    'Platinum',


    'AspenLog',
    'CedarLog',
    'MapleLog',

    'CopperOre',
    'NickelOre',
    'IronOre',

    'RawSalmon',
    'RawTuna',
    'RawBass',


    'CuSword',
    'FeSword',
    'NiSword',

    'CuShield',
    'FeShield',
    'NiShield',

    'CuArrow',
    'FeArrow',
    'NiArrow',

    'CuAxe',
    'FeAxe',
    'NiAxe',

    'CuPick',
    'FePick',
    'NiPick',


    'AsHilt',
    'AsFrame',
    'AsShaft',
    'AsHandle',

    'CeHilt',
    'CeFrame',
    'CeShaft',
    'CeHandle',

    'MaHilt',
    'MaFrame',
    'MaShaft',
    'MaHandle',



    'AsCuSword',
    'CeCuSword',
    'MaCuSword',

    'AsFeSword',
    'CeFeSword',
    'MaFeSword',

    'AsNiSword',
    'CeNiSword',
    'MaNiSword',


    'AsCuShield',
    'CeCuShield',
    'MaCuShield',

    'AsFeShield',
    'CeFeShield',
    'MaFeShield',

    'AsNiShield',
    'CeNiShield',
    'MaNiShield',


    'AsCuArrow',
    'CeCuArrow',
    'MaCuArrow',

    'AsFeArrow',
    'CeFeArrow',
    'MaFeArrow',

    'AsNiArrow',
    'CeNiArrow',
    'MaNiArrow',


    'AsCuAxe',
    'CeCuAxe',
    'MaCuAxe',

    'AsFeAxe',
    'CeFeAxe',
    'MaFeAxe',

    'AsNiAxe',
    'CeNiAxe',
    'MaNiAxe',


    'AsCuPick',
    'CeCuPick',
    'MaCuPick',

    'AsFePick',
    'CeFePick',
    'MaFePick',

    'AsNiPick',
    'CeNiPick',
    'MaNiPick'
)

item_dictionary[ItemDictionaryIndex.Copper] = Item("Copper", 1, True)
item_dictionary[ItemDictionaryIndex.Silver] = Item("Silver", 1000, True)
item_dictionary[ItemDictionaryIndex.Gold] = Item("Gold", 1000 * 1000, True)
item_dictionary[ItemDictionaryIndex.Platinum] = Item("Platinum", 1000 * 1000 * 1000, True)

item_dictionary[ItemDictionaryIndex.AspenLog] = Item("Aspen Log", 75, False)
item_dictionary[ItemDictionaryIndex.CedarLog] = Item("Cedar Log", 75, False)
item_dictionary[ItemDictionaryIndex.MapleLog] = Item("Maple Log", 75, False)

item_dictionary[ItemDictionaryIndex.CopperOre] = Item("Copper Ore", 75, False)
item_dictionary[ItemDictionaryIndex.NickelOre] = Item("Nickel Ore", 75, False)
item_dictionary[ItemDictionaryIndex.IronOre] = Item("Iron Ore", 75, False)

item_dictionary[ItemDictionaryIndex.RawSalmon] = Item("Raw Salmon", 75, False, 2, -1, 1)
item_dictionary[ItemDictionaryIndex.RawTuna] = Item("Raw Tuna", 75, False, 2, -1, 2)
item_dictionary[ItemDictionaryIndex.RawBass] = Item("Raw Bass", 75, False, 2, -1, 3)

item_dictionary[ItemDictionaryIndex.CuSword] = Item("Cu Sword", 115, False)
item_dictionary[ItemDictionaryIndex.FeSword] = Item("Fe Sword", 115, False)
item_dictionary[ItemDictionaryIndex.NiSword] = Item("Ni Sword", 115, False)

item_dictionary[ItemDictionaryIndex.CuShield] = Item("Cu Shield", 115, False)
item_dictionary[ItemDictionaryIndex.FeShield] = Item("Fe Shield", 115, False)
item_dictionary[ItemDictionaryIndex.NiShield] = Item("Ni Shield", 115, False)

item_dictionary[ItemDictionaryIndex.CuArrow] = Item("Cu Arrow", 115, True)
item_dictionary[ItemDictionaryIndex.FeArrow] = Item("Fe Arrow", 115, True)
item_dictionary[ItemDictionaryIndex.NiArrow] = Item("Ni Arrow", 115, True)

item_dictionary[ItemDictionaryIndex.CuAxe] = Item("Cu Axe", 115, False)
item_dictionary[ItemDictionaryIndex.FeAxe] = Item("Fe Axe", 115, False)
item_dictionary[ItemDictionaryIndex.NiAxe] = Item("Ni Axe", 115, False)

item_dictionary[ItemDictionaryIndex.CuPick] = Item("Cu Pick", 115, False)
item_dictionary[ItemDictionaryIndex.FePick] = Item("Fe Pick", 115, False)
item_dictionary[ItemDictionaryIndex.NiPick] = Item("Ni Pick", 115, False)

item_dictionary[ItemDictionaryIndex.AsHilt] = Item("As Hilt", 115, False)
item_dictionary[ItemDictionaryIndex.AsFrame] = Item("As Frame", 115, False)
item_dictionary[ItemDictionaryIndex.AsShaft] = Item("As Shaft", 115, True)
item_dictionary[ItemDictionaryIndex.AsHandle] = Item("As Handle", 115, False)

item_dictionary[ItemDictionaryIndex.CeHilt] = Item("Ce Hilt", 115, False)
item_dictionary[ItemDictionaryIndex.CeFrame] = Item("Ce Frame", 115, False)
item_dictionary[ItemDictionaryIndex.CeShaft] = Item("Ce Shaft", 115, True)
item_dictionary[ItemDictionaryIndex.CeHandle] = Item("Ce Handle", 115, False)

item_dictionary[ItemDictionaryIndex.MaHilt] = Item("Ma Hilt", 115, False)
item_dictionary[ItemDictionaryIndex.MaFrame] = Item("Ma Frame", 115, False)
item_dictionary[ItemDictionaryIndex.MaShaft] = Item("Ma Shaft", 115, True)
item_dictionary[ItemDictionaryIndex.MaHandle] = Item("Ma Handle", 115, False)



item_dictionary[ItemDictionaryIndex.AsCuSword] = Item("AsCu Sword", 115, False, 1, 8, 2)
item_dictionary[ItemDictionaryIndex.CeCuSword] = Item("CeCu Sword", 115, False, 1, 8, 3)
item_dictionary[ItemDictionaryIndex.MaCuSword] = Item("MaCu Sword", 115, False, 1, 8, 4)

item_dictionary[ItemDictionaryIndex.AsFeSword] = Item("AsFe Sword", 115, False, 1, 8, 5)
item_dictionary[ItemDictionaryIndex.CeFeSword] = Item("CeFe Sword", 115, False, 1, 8, 6)
item_dictionary[ItemDictionaryIndex.MaFeSword] = Item("MaFe Sword", 115, False, 1, 8, 7)

item_dictionary[ItemDictionaryIndex.AsNiSword] = Item("AsNi Sword", 115, False, 1, 8, 8)
item_dictionary[ItemDictionaryIndex.CeNiSword] = Item("CeNi Sword", 115, False, 1, 8, 9)
item_dictionary[ItemDictionaryIndex.MaNiSword] = Item("MaNi Sword", 115, False, 1, 8, 10)


item_dictionary[ItemDictionaryIndex.AsCuShield] = Item("AsCu Shield", 115, False, 1, 9, 2)
item_dictionary[ItemDictionaryIndex.CeCuShield] = Item("CeCu Shield", 115, False, 1, 9, 3)
item_dictionary[ItemDictionaryIndex.MaCuShield] = Item("MaCu Shield", 115, False, 1, 9, 4)

item_dictionary[ItemDictionaryIndex.AsFeShield] = Item("AsFe Shield", 115, False, 1, 9, 5)
item_dictionary[ItemDictionaryIndex.CeFeShield] = Item("CeFe Shield", 115, False, 1, 9, 6)
item_dictionary[ItemDictionaryIndex.MaFeShield] = Item("MaFe Shield", 115, False, 1, 9, 7)

item_dictionary[ItemDictionaryIndex.AsNiShield] = Item("AsNi Shield", 115, False, 1, 9, 8)
item_dictionary[ItemDictionaryIndex.CeNiShield] = Item("CeNi Shield", 115, False, 1, 9, 9)
item_dictionary[ItemDictionaryIndex.MaNiShield] = Item("MaNi Shield", 115, False, 1, 9, 10)


item_dictionary[ItemDictionaryIndex.AsCuArrow] = Item("AsCu Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.CeCuArrow] = Item("CeCu Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.MaCuArrow] = Item("MaCu Arrow", 115, True, 1, 2)

item_dictionary[ItemDictionaryIndex.AsFeArrow] = Item("AsFe Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.CeFeArrow] = Item("CeFe Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.MaFeArrow] = Item("MaFe Arrow", 115, True, 1, 2)

item_dictionary[ItemDictionaryIndex.AsNiArrow] = Item("AsNi Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.CeNiArrow] = Item("CeNi Arrow", 115, True, 1, 2)
item_dictionary[ItemDictionaryIndex.MaNiArrow] = Item("MaNi Arrow", 115, True, 1, 2)


item_dictionary[ItemDictionaryIndex.AsCuAxe] = Item("AsCu Axe", 115, False)
item_dictionary[ItemDictionaryIndex.CeCuAxe] = Item("CeCu Axe", 115, False)
item_dictionary[ItemDictionaryIndex.MaCuAxe] = Item("MaCu Axe", 115, False)

item_dictionary[ItemDictionaryIndex.AsFeAxe] = Item("AsFe Axe", 115, False)
item_dictionary[ItemDictionaryIndex.CeFeAxe] = Item("CeFe Axe", 115, False)
item_dictionary[ItemDictionaryIndex.MaFeAxe] = Item("MaFe Axe", 115, False)

item_dictionary[ItemDictionaryIndex.AsNiAxe] = Item("AsNi Axe", 115, False)
item_dictionary[ItemDictionaryIndex.CeNiAxe] = Item("CeNi Axe", 115, False)
item_dictionary[ItemDictionaryIndex.MaNiAxe] = Item("MaNi Axe", 115, False)


item_dictionary[ItemDictionaryIndex.AsCuPick] = Item("AsCu Pick", 115, False)
item_dictionary[ItemDictionaryIndex.CeCuPick] = Item("CeCu Pick", 115, False)
item_dictionary[ItemDictionaryIndex.MaCuPick] = Item("MaCu Pick", 115, False)

item_dictionary[ItemDictionaryIndex.AsFePick] = Item("AsFe Pick", 115, False)
item_dictionary[ItemDictionaryIndex.CeFePick] = Item("CeFe Pick", 115, False)
item_dictionary[ItemDictionaryIndex.MaFePick] = Item("MaFe Pick", 115, False)

item_dictionary[ItemDictionaryIndex.AsNiPick] = Item("AsNi Pick", 115, False)
item_dictionary[ItemDictionaryIndex.CeNiPick] = Item("CeNi Pick", 115, False)
item_dictionary[ItemDictionaryIndex.MaNiPick] = Item("MaNi Pick", 115, False)