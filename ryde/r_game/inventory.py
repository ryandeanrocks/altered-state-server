
import Queue
from item import *


class ItemSlot:

    next_world_item_id = 0

    def __init__(self, base=None, spawned=False):
        self.base = base
        self.stack = Queue.Queue()
        self.stack.maxsize = Item.maxStack

        self.spawned = spawned

        self.world_id = ItemSlot.next_world_item_id
        ItemSlot.next_world_item_id += 1

        self.world_position = [0.0, 0.0, 0.0]
        self.dropLife = 60

    def holds_item(self, name):
        if self.base.name == name.rstrip().lstrip():
            return True
        return False

    def add_to_stack(self, instance, allStack=False):
        if self.base is None:
            self.base = instance.base

        if self.base != instance.base:
            return False

        if not self.base.stackable and self.quantity() > 0 and not allStack:
            return False

        if not (self.stack.full()):
            self.stack.put(instance)
            return True
        return False

    def take_from_stack(self):
        instance = None
        if not (self.stack.empty()):
            instance = self.stack.get()
            #print instance.base.name + " 1"
            if self.stack.empty():
                self.base = None

            #print instance.base.name + " 2"
        return instance

    def quantity(self):
        return self.stack.qsize()

    def tick(self):
        if not self.spawned:
            self.dropLife -= 1
            if self.dropLife <= 0:
                self.dropLife = 60
                self.take_from_stack()



class Inventory:

    def __init__(self, name, limit, allStack=False, equip=False):
        self.name = name
        self.slots = []
        self.limit = limit

        self.allStack = allStack
        self.equip = equip

        for s in range(0,limit):
            self.slots.append(ItemSlot(None))


    def hasItem(self, name, count=1):
        slots = []
        for i, s in enumerate(self.slots):
            if s.base is not None:
                #print "Base: %s Name: %s" % (s.base.name, name)

                if s.holds_item(name):
                    #item = s.take_from_stack()
                    #print "removing item %s" % item.base.name
                    slots.append(i)
                    count -= 1
                    if count == 0:
                        if len(slots) > 1:
                            return slots
                        else:
                            return i
        return None


    def addItem(self, instance):
        if instance is not None:
            #print "Adding %s to %s" % (instance.base.name, self.name)

            if self.equip:
                if instance.base.equip > -1:
                    s = self.slots[instance.base.equip]
                    if s.add_to_stack(instance, self.allStack):
                        # print("Item added to slot")
                        return instance.base.equip
            else:

                for i, s in enumerate(self.slots):
                    if s.base is not None:
                        if s.add_to_stack(instance, self.allStack):
                            #print("Item added to slot")
                            return i

                for i, s in enumerate(self.slots):
                    if s.add_to_stack(instance, self.allStack):
                        #print("Item added to slot")
                        return i
        return None

    def removeItem(self, name, count=1):
        items = []
        for s in self.slots:
            if s.base is not None:
                #print "Base: %s Name: %s" % (s.base.name, name)

                if s.holds_item(name):
                    item = s.take_from_stack()
                    items.append(item)
                    count -= 1
                    #print "removing item %s" % item.base.name
                    if count <= 0:
                        if len(items) > 1:
                            return items
                        else:
                            return item
        return None



    def to_terminal(self):
        print "Inventory %s" % self.name
        for i, s in enumerate(self.slots):
            if s.base is not None:
                print "[%d] Item: %s Stackable: %s Stack: %d Value: %d" % (
                i, s.base.name, s.base.stackable, s.quantity(), s.base.value)