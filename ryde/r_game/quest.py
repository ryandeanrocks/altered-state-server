

class Quest:

    def __init__(self, name, stage=0, step=0):
        self.name = name
        self.stage = stage
        self.step = step

    def state(self):
        return "{\"state\":{\"quest\":{\"name\":\"%s\",\"stage\":%d,\"step\":%d}}}" % (self.name, self.stage, self.step)