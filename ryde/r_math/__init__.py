import math
import random


def randomFloat(min, max):
    return float(random.randint(int(min * 1000), int(max * 1000))) / 1000.0

def calculateDistanceDirection(source, target):
    dist = math.sqrt((target[0] - source[0]) ** 2 + (target[1] - source[1]) ** 2)
    if dist == 0.0:
        return 0.0, [0.0, 0.0]
    return dist, [-((target[0] - source[0]) / dist), -((target[1] - source[1]) / dist)]


def clamp(n, minn, maxn):
    return max(min(maxn, n), minn)
