===============
Ryde Framework
===============
*Python framework for use with RydeTec Python applications.*

Ryde Python Framework is a set of modules that are to be used with Ryde Python applications. Applications should only make references to the framework and should never have to connect to the system directly.


