from threading import Thread
import threading


class RyThread:
    def __init__(self, func, arg):
        self.func = func
        self.arg = arg
        self.thread = Thread(target=self.func, args=self.arg)
        self.thread.start()

    def is_alive(self):
        return self.thread.isAlive()

    def run(self):
        if not self.is_alive():
            self.thread = Thread(target=self.func, args=self.arg)
            self.thread.start()

    def main_alive(self):
        return any((i.name == "MainThread") and i.is_alive() for i in threading.enumerate())
