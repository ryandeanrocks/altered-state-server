
import datetime
import time


class When:

    def __init__(self, timestamp = None):
        self.string = datetime.datetime.now
        self.milli = lambda: int(round(time.time() * 1000))

