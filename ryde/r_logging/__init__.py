
from ryde.r_spacetime import *
from enum import Enum
import os
import inspect


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Debug:

    def __init__(self):
        print "DEBUG INIT NOT NEEDED"

    @staticmethod
    def line_number():
        return inspect.currentframe().f_back.f_lineno

    @staticmethod
    def file_name():
        return inspect.getframeinfo(inspect.currentframe()).filename

class Log:

    class EntryType(Enum):
        INFO = 1
        WARN = 2
        ERROR = 3

    def __init__(self, name, debug=True, supressWarnings=False, record=True):
        self.name = name
        self.file = "%s-%s.log" % (name, When().string())

        self.debug = debug
        self.supressWarnings = supressWarnings
        self.record = record

    def entry(self, message, et = EntryType.INFO):
        type_string = "INFO"
        if et == self.EntryType.WARN:
            type_string = "WARNING"
        if et == self.EntryType.ERROR:
            type_string = "ERROR"

        logMessage = "[%s] [%s] %s" % (When().string(), type_string, message)

        if not os.path.exists('logs'):
            os.makedirs('logs')

        if self.debug or et == 3 or (et == 2 and not self.supressWarnings):
            debugMessage = " [%s %d]" % (inspect.getframeinfo(inspect.currentframe().f_back).filename,
                                         inspect.currentframe().f_back.f_lineno)
            logMessage = logMessage + debugMessage

            if et == 2:
                print bcolors.WARNING + self.name + " " + logMessage + bcolors.ENDC
            elif et == 3:
                print bcolors.FAIL + self.name + " " + logMessage + bcolors.ENDC
            else:
                print self.name + " " + logMessage

        if self.record:
            f = open('logs/%s' % self.file, 'a')
            f.write('\n' + logMessage)
            f.close()