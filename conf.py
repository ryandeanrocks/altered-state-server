import re

import sphinx

from recommonmark.parser import CommonMarkParser

source_parsers = {
    '.md': CommonMarkParser,
}

source_suffix = ['.rst', '.md']

project = 'Altered State'
author = 'RydeTec'
copyright = '2018 RydeTec'
version = sphinx.__display_version__
release = version

html_sidebars = {
   '**': ['globaltoc.html', 'sourcelink.html', 'searchbox.html']
}